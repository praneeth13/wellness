import React, { Component } from 'react';
import logoImage from '../assets/images/logo-web.png'
import '../assets/css/theme.css'
import '../assets/css/skins/default.css';
//import Select from "react-select";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import * as authAction from "../actions/auth.action";
import * as userAction from "../actions/user.action";
import { headers } from "../config";
import { ToastContainer, toast } from 'react-toastify';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFlagUsa } from '@fortawesome/free-solid-svg-icons';
import { faAmericanSignLanguageInterpreting } from '@fortawesome/free-solid-svg-icons';
import { faBook } from '@fortawesome/free-solid-svg-icons';
import Header from '../layout/header/header'


export class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
    
    };
  }


  render() {
    return (
        <div>
            <Header></Header>
            <section role="main" className="content-body">
                    <div className="inner-wrapper">
                        <div className="row">
                            <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 px-5" id="vlis-welcome">
                                <h1 className="font-weight-bold text-uppercase">welcome</h1>
                                <p className="text-dark text-5 line-height-xl font-weight-light">Wellness is a Covid app for weellness of the Society, by monitoring the user's social distancing</p>
                            </div>
                            <div className="col-xl-9 col-lg-9 col-md-12 col-sm-12" id="vlis-users">
                                <div className="row px-3">
                                   
                                {/* <div className="col-xl-6">
                                    <section

                                    className="card mb-3 transition-2ms box-shadow-1 box-shadow-1-success box-shadow-1-hover">
                                    <div className="card-body bg-blue bg-color-white-scale-3" id="vlis-engineer">
                                        <div className="widget-summary">
                                        <div className="widget-summary-col widget-summary-col-icon">
                                            <div className="summary-icon bg-white">
                                            <i class="fa fa-desktop fa-bg-transparent"  aria-hidden="true"></i>

                                            </div>
                                        </div>
                                        <div className="widget-summary-col">
                                            <div className="summary">
                                            <div className="info">
                                                <strong className="amount text-light">Dashboard</strong>
                                            </div>
                                            <p className="mb-0 my-3 line-height-xs text-light">Translators create translation media in the editor workspace. They also have the responsibility to revise existing media.</p>
                                            </div>
                                            <div className="summary-footer">
                                            <div class="form-group row mt-3">
                                            </div>
                                            <div class="form-group row pull-right mr-1">
                                                <button
                                                onClick={e => {
                                                    this.props.history.push('/dashboard')
                                                }}
                                                className="btn btn-dark btn-sm text-light">
                                                Get Started
                                                </button>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </section>
                                </div> */}
                                <div className="col-xl-6">
                                <section className="card mb-3 transition-2ms box-shadow-1 box-shadow-1-primary box-shadow-1-hover">
                                            <div className="card-body homepageCard bg-color-dark-scale-3  " id="vlis-translate-admin">
                                                <div className="widget-summary">
                                                    <div className="widget-summary-col widget-summary-col-icon">
                                                        <div className="summary-icon bg-white">
                                                        <i class="fa fa-desktop fa-bg-transparent"  aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                    <div className="widget-summary-col">
                                                        <div className="summary">
                                                            <div className="white">
                                                                <strong className="amount text-light">Dashboard</strong>
                                                            </div>
                                                            <p className="mb-0 my-3 line-height-xs text-light">Translators create translation media in the editor workspace. They also have the responsibility to revise existing media.</p>
                                                        </div>
                                                        <div className="summary-footer">
                                                            <div class="form-group row pull-right mr-1">
                                                                <button
                                                                className="btn btn-dark btn-sm text-white"
                                                                onClick={e => {
                                                                    this.props.history.push('/dashboard')
                                                                }}>
                                                                Get Started
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                </div>
                                <div className="col-xl-6">
                                <section className="card mb-3 transition-2ms box-shadow-1 box-shadow-1-primary box-shadow-1-hover">
                                            <div className="card-body homepageCard bg-color-dark-scale-3  " id="vlis-translate-admin">
                                                <div className="widget-summary">
                                                    <div className="widget-summary-col widget-summary-col-icon">
                                                        <div className="summary-icon bg-white">
                                                        <i class="fas fa-capsules text-dark"></i>
                                                        </div>
                                                    </div>
                                                    <div className="widget-summary-col">
                                                        <div className="summary">
                                                            <div className="white">
                                                                <strong className="amount text-light">Policy Builder</strong>
                                                            </div>
                                                            <p className="mb-0 my-3 line-height-xs text-light">Translators create translation media in the editor workspace. They also have the responsibility to revise existing media.</p>
                                                        </div>
                                                        <div className="summary-footer">
                                                            <div class="form-group row pull-right mr-1">
                                                                <button
                                                                className="btn btn-dark btn-sm text-white"
                                                                onClick={e => {
                                                                    this.props.history.push('/rules')
                                                                }}>
                                                                Get Started
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                </div>
                                <div className="col-xl-6">
                                <section className="card mb-3 transition-2ms box-shadow-1 box-shadow-1-primary box-shadow-1-hover">
                                            <div className="card-body homepageCard bg-color-dark-scale-3 " id="vlis-translate-admin">
                                                <div className="widget-summary">
                                                    <div className="widget-summary-col widget-summary-col-icon">
                                                        <div className="summary-icon bg-white ">
                                                        <i class="fa fa-exclamation-triangle  fa-bg-transparent" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                    <div className="widget-summary-col">
                                                        <div className="summary">
                                                            <div className="white">
                                                                <strong className="amount text-light">Reports and Alerts</strong>
                                                            </div>
                                                            <p className="mb-0 my-3 line-height-xs text-light">Translators create translation media in the editor workspace. They also have the responsibility to revise existing media.</p>
                                                        </div>
                                                        <div className="summary-footer">
                                                            <div class="form-group row pull-right mr-1">
                                                                <button
                                                                className="btn btn-dark btn-sm text-white"
                                                                onClick={e => {
                                                                    this.props.history.push('/admin')
                                                                }}>
                                                                Get Started
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                </div>
                                <div className="col-xl-6">
                                <section className="card mb-3 transition-2ms box-shadow-1 box-shadow-1-primary box-shadow-1-hover">
                                            <div className="card-body homepageCard bg-color-dark-scale-3 " id="vlis-translate-admin">
                                                <div className="widget-summary">
                                                    <div className="widget-summary-col widget-summary-col-icon">
                                                        <div className="summary-icon bg-white">
                                                        <i class="fas fa-globe-americas text-dark"></i>
                                                        </div>
                                                    </div>
                                                    <div className="widget-summary-col">
                                                        <div className="summary">
                                                            <div className="white">
                                                                <strong className="amount text-light">Map View</strong>
                                                            </div>
                                                            <p className="mb-0 my-3 line-height-xs text-light">Translators create translation media in the editor workspace. They also have the responsibility to revise existing media.</p>
                                                        </div>
                                                        <div className="summary-footer">
                                                            <div class="form-group row pull-right mr-1">
                                                                <button
                                                                className="btn btn-dark btn-sm text-white"
                                                                onClick={e => {
                                                                    this.props.history.push('/admin')
                                                                }}>
                                                                Get Started
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                </div>
                                <div className=" col-xl-6">
                                    <section className="card mb-3 transition-2ms box-shadow-1 box-shadow-1-primary box-shadow-1-hover">
                                            <div className="card-body homepageCard bg-color-dark-scale-3 " id="vlis-translate-admin">
                                                <div className="widget-summary">
                                                    <div className="widget-summary-col widget-summary-col-icon">
                                                        <div className="summary-icon bg-white">
                                                          <i class="fa fa-tasks fa-bg-transparent" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                    <div className="widget-summary-col">
                                                        <div className="summary">
                                                            <div className="white">
                                                                <strong className="amount text-light">Datasets Management</strong>
                                                            </div>
                                                            <p className="mb-0 my-3 line-height-xs text-light">Translators create translation media in the editor workspace. They also have the responsibility to revise existing media.</p>
                                                        </div>
                                                        <div className="summary-footer">
                                                            <div class="form-group row pull-right mr-1">
                                                                <button
                                                                className="btn btn-dark btn-sm text-white"
                                                                onClick={e => {
                                                                    this.props.history.push('/admin')
                                                                }}>
                                                                Get Started
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                <div className="col-xl-6">
                               
                                        <section className="card mb-3 transition-2ms box-shadow-1 box-shadow-1-primary box-shadow-1-hover">
                                            <div className="card-body homepageCard bg-color-dark-scale-3 " id="vlis-translate-admin">
                                                <div className="widget-summary">
                                                    <div className="widget-summary-col widget-summary-col-icon">
                                                        <div className="summary-icon bg-white">
                                                        <i class="fas fa-user-md text-dark"></i>
                                                         </div>
                                                    </div>
                                                    <div className="widget-summary-col">
                                                        <div className="summary">
                                                            <div className="white">
                                                                <strong className="amount text-light">Administrator</strong>
                                                            </div>
                                                            <p className="mb-0 my-3 line-height-xs text-light">Translators create translation media in the editor workspace. They also have the responsibility to revise existing media.</p>
                                                        </div>
                                                        <div className="summary-footer">
                                                            <div class="form-group row pull-right mr-1">
                                                                <button
                                                                className="btn btn-dark btn-sm text-white"
                                                                onClick={e => {
                                                                    this.props.history.push('/admin')
                                                                }}>
                                                                Get Started
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ToastContainer />
            </section>
        </div>     
    );
  }
}






export default Home;

