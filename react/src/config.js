

let baseUrl, baseUrlApi;

// Change this value if you wish to connect to a remote QA/Prod server.
let local = true;

if (local) {
    // Local settings - do not change these.
    // baseUrlApi = 'https://f424a4ee937c.ngrok.io';
    baseUrl = 'http://127.0.0.1:3001/wellness';
    baseUrlApi = 'http://127.0.0.1:3001';


} else {
    // Remote settings - change to desired QA/Prod server values.
    // Make sure 'local' is set to false above.
    //   baseUrl = 'https://avodah-vlis-web-api.appserve.io';
    //   baseUrlApi = 'https://avodah-vlis-ml-service.appserve.io';
    //   Optisol cloud url
    //  baseUrl ='https://vlis.optisolbusiness.com/backend';
    //  baseUrlApi = 'https://vlis.optisolbusiness.com/python';
}

const headers = { headers: { 
    Authorization: "JWT " + localStorage.getItem("wellness-token"),
    "Access-Control-Allow-Origin": "*",
} }


export { baseUrl, baseUrlApi, headers };
