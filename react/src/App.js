import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import Login from './login/login';
import Register from './login/register';
import ForgotPassword from './forgot-password/forgot-password';
import ResetPassword from './reset-password/reset-password';
import { Provider } from "react-redux";
import store from "./store";
import './App.scss';
import Dashboard from './components/dashboard'
import Layout from './layout/layout';
import ChangePassword from './change-password/change-password';
import Admin from './components/admin';
import Profile from './components/profile';
import Map from './components/Map';
import Viewfeed from './components/ViewFeed';
import { history } from './_helpers/history'
import Home from './home/home';
import LocalCam from './components/LocalCam'
import Matrics from "./components/matrics";
import Rules from "./components/rules";
import Leaflet from "./components/mapview";
import Events from './components/events';
require('dotenv').config()


// const App = () => (
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenu: false
    };
    this.stateChange = this.stateChange;
  }

  stateChange = (showMenu) => {
    this.setState({
      showMenu: showMenu
    });
  };
  render() {
    let sidebarClass='  sidebaropen'
    return (
      <Provider store={store}>
        <Router history={history}>
          <div>
            <main>
              <Switch>

                <Route exact path="/" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route path="/forgot-password" component={ForgotPassword} />
                <Route path="/reset-password" component={ResetPassword} />
                <Route path="/home" component={Home} />
                <Route exact={true} path='/dashboard' render={() => (
                  <div>
                    <Layout stateChange={this.stateChange} />
                    <div className={'pt67 pr-2 ' +sidebarClass }>
                      <Dashboard history={history} showMenu={this.state.showMenu} />

                    </div>
                  </div>
                )} />
                <Route exact={true} path='/localcam' render={() => (
                  <div>
                    <Layout stateChange={this.stateChange} />
                    <div className={'pt67 pr-2 ' + sidebarClass}>
                      <LocalCam showMenu={this.state.showMenu} />
                    </div>
                  </div>
                )} />
                 <Route exact={true} path='/rules' render={() => (
                  <div>
                    <Layout stateChange={this.stateChange} />
                    <div className={'pt67 pr-2 ' + sidebarClass}>
                      <Rules showMenu={this.state.showMenu} />
                    </div>
                  </div>
                )} />
                <Route exact={true} path='/matrics' render={() => (
                  <div>
                    <Layout stateChange={this.stateChange} />
                    <div className={'pt67 pr-2 ' + sidebarClass}>
                      <Matrics showMenu={this.state.showMenu} />
                    </div>
                  </div>
                )} />
                <Route exact={true} path='/Leaflet' render={() => (
                  <div>
                    <Layout stateChange={this.stateChange} />
                    <div className={'pt67 pr-2 ' + sidebarClass}>
                      <Leaflet history={history} showMenu={this.state.showMenu} />
                    </div>
                  </div>
                )} />
                <Route exact={true} path='/map' render={() => (
                  <div>
                    <Layout stateChange={this.stateChange} />
                    <div className={'pt67 pr-2 ' + sidebarClass}>
                      <Map history={history} showMenu={this.state.showMenu} />
                    </div>
                  </div>
                )} />
                <Route exact={true} path='/viewfeed' render={() => (
                  <div>
                    <Layout stateChange={this.stateChange} />
                    <div className={'pt67 pr-2 ' + sidebarClass}>
                      <Viewfeed history={history} showMenu={this.state.showMenu} />
                    </div>
                  </div>
                )} />
                <Route exact={true} path='/events' render={() => (
                  <div>
                    <Layout stateChange={this.stateChange} />
                    <div className={'pt67 pr-2 ' + sidebarClass}>
                      <Events history={history} showMenu={this.state.showMenu} />
                    </div>
                  </div>
                )} />
                <Route exact={true} path='/change-password' render={() => (
                  <div>
                    <Layout stateChange={this.stateChange} />
                    <div className={'pt67 pr-2 ' + sidebarClass}>
                      <ChangePassword history={history} showMenu={this.state.showMenu} />
                    </div>
                  </div>
                )} />
                <Route exact={true} path='/admin' render={() => (
                  <div>
                    <Layout stateChange={this.stateChange} />
                    <div className={'pt67 pr-2 ' + sidebarClass}>
                      <Admin showMenu={this.state.showMenu} />
                    </div>
                  </div>
                )} />

                <Route exact={true} path='/profile' render={() => (
                  <div>
                    <Layout stateChange={this.stateChange} />
                    <div className={'pt67 pr-2 ' + sidebarClass}>
                      <Profile history={history} showMenu={this.state.showMenu} />
                    </div>
                  </div>
                )} />
              </Switch>
            </main>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;