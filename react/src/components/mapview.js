import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { Map, Marker, Popup, TileLayer } from 'react-leaflet'

class Mapview extends PureComponent {
    constructor(props) {
        super(props)

        this.state = {

        }
    }

    render() {
        const position = [51.505, -0.09]
        let url=('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            maxZoom: 10,
            draggable: true,
            id: 'mapbox.streets',
            accessToken:
              'pk.eyJ1IjoicHJvdG9jb2w3IiwiYSI6ImNrMjUxZjEzYTAwbXYzbHRscnE5dDFnOWQifQ.uOVge_4xINbcWwQ8gA-rGg',
          })
          console.log(url)
        return (
            <section role="main" className="content-body">
            <header className={'page-header ' + (this.state.showMenu ? '' : 'header-left')}>
                <h2>Map 2</h2>
            </header>
            <div className="row">sdsd
            <Map center={position} zoom={13}>
                    <TileLayer
                         attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                         url={url}
                    />
                    <Marker position={position}>
                        <Popup>A pretty CSS3 popup.<br />Easily customizable.</Popup>
                    </Marker>
                </Map>
            </div>
        </section>
           
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(Mapview)
