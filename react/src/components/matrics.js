import React, { Component, useState } from 'react'
import { connect } from "react-redux";
// import { Chart } from 'react-charts'
import Chart from "react-google-charts";
import Axios from 'axios';
// import 'react-date-range/dist/styles.css'; 
// import 'react-date-range/dist/theme/default.css';
// import { DateRange } from 'react-date-range'; 
// import { DateRangePicker } from 'react-date-range';
import DateRangePicker from '@wojtekmaj/react-daterange-picker';
import '../assets/css/chart.css';
class Matrics extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: new Date(),
            endDate: new Date(),
        }    
    }

    patients = (data) => {
        return data.reduce(
            (accumulatedLength, data) =>
                accumulatedLength + data.data.length
            , 0)
    }

    handleSelect = (ranges) => {
        console.log(ranges);
        // this.setState
        this.setState(ranges)
    }

    componentDidMount(){
        Axios.get(`https://f0ab61bdbfe6.ngrok.io/violations`).then(res => {
            console.log(res)
        })
    }

    handleSelect = (range) => {
        console.log(range);
        this.setState({
            startDate : range[0],
            endDate : range[1]
        })
    }

    render() {
        const data = [
            {
                label: 'Series 1',
                city: 'chennai',
                data: [[0, 1], [1, 2], [2, 4], [3, 2], [4, 7]]
            },
            {
                label: 'Series 2',
                city: 'Coimbatore',
                data: [[0, 3], [1, 1], [2, 5], [3, 6], [4, 4]]
            },
            {
                label: 'Series 3',
                city: 'Delhi',
                data: [[2, 0], [2, 7], [3, 3], [3, 6], [4, 7]]
            }
        ]


        const axes = [
            { primary: true, type: 'linear', position: 'bottom' },
            { type: 'linear', position: 'left' }
        ]

        const axes1 = [
            { primary: true, type: 'ordinal', position: 'left' },
            { position: 'bottom', type: 'linear', stacked: true }
        ]

        const axes2 = [
            { primary: true, type: 'time', position: 'bottom' },
            { type: 'linear', position: 'left' }
        ]

        const axes3 = [
            { primary: true, type: 'ordinal', position: 'bottom' },
            { type: 'linear', position: 'left' }
        ]



        const series = ({
            type: 'bar'
        })

        const selectionRange = {
            startDate: this.state.startDate,
            endDate: this.state.endDate,
            key: 'selection',
        }
        const value = [this.state.startDate,this.state.endDate]
        return (
            <section role="main" className="content-body">

                <header className={'page-header ' + (this.state.showMenu ? '' : 'header-left')}>
                    <h2>Metrics</h2>
                </header>
                {/* <div className= "container"> */}
                <div className="row">
                    <div className="col-12 graph-wrapper">
                        <h3>1. Wearing  coverings - all locations</h3>
                        <label for="city" >City Filter </label><br />
                        <select className="selectCity" id="city" name="city">
                            <option value="none">Select City</option>
                            {
                                data.map((value, index) => {
                                    return (
                                        <option key={index} value={value.city}>
                                            {value.city}
                                        </option>
                                    )
                                })
                            }
                        </select>

                        <DateRangePicker
                            onChange={this.handleSelect}
                            value={value}
                        />
                        <br />
                        <section className="card chart-bottom">
                            <div className="card-body chart-body ">
                            {/* <Chart data={data} axes={axes1} tooltip /> */}
                            <Chart
                                chartType="LineChart"
                                loader={<div>Loading Chart</div>}
                                data={[
                                    ['x', 'People'],
                                    [new Date( 2020, 0, 1), 5],  [new Date( 2020, 0, 2), 7],  [new Date( 2020, 0, 3), 3],
                                    [new Date( 2020, 0, 4), 1],  [new Date( 2020, 0, 5), 3],  [new Date( 2020, 0, 6), 4],
                                    [new Date( 2020, 0, 7), 3],  [new Date( 2020, 0, 8), 4],  [new Date( 2020, 0, 9), 2],
                                    [new Date( 2020, 0, 10), 5], [new Date( 2020, 0, 11), 8], [new Date( 2020, 0, 12), 6],
                                    [new Date( 2020, 0, 13), 3], [new Date( 2020, 0, 14), 3], [new Date( 2020, 0, 15), 5],
                                    [new Date( 2020, 0, 16), 7], [new Date( 2020, 0, 17), 6], [new Date( 2020, 0, 18), 6],
                                    [new Date( 2020, 0, 19), 3], [new Date( 2020, 0, 20), 1], [new Date( 2020, 0, 21), 2],
                                    [new Date( 2020, 0, 22), 4], [new Date( 2020, 0, 23), 6], [new Date( 2020, 0, 24), 5],
                                    [new Date( 2020, 0, 25), 9], [new Date( 2020, 0, 26), 4], [new Date( 2020, 0, 27), 9],
                                    [new Date( 2020, 0, 28), 8], [new Date( 2020, 0, 29), 6], [new Date( 2020, 0, 30), 4],
                                    [new Date( 2020, 0, 31), 6], [new Date( 2020, 1, 1), 7],  [new Date( 2020, 1, 2), 9]
                                ]}
                                options={{
                                    title: 'No. of people not wearing masks on a given date',
                                    backgroundColor: 'transparent',
                                    width: '100%',
                                    height: '100%',
                                    hAxis: {
                                      format: 'M/d/yy',
                                      gridlines: {count: 15}
                                    },
                                    vAxis: {
                                      gridlines: {color: 'none'},
                                      minValue: 0
                                    }
                                  }}
                                rootProps={{ 'data-testid': '1' }}
                            />
                            </div>
                        </section>
                    </div>
                    {/* <div className="col-xl-6 col-lg-3 col-md-4 col-sm-4 graph-wrapper ">
                        <h3>2. Wearing  coverings - filtered by locations</h3>
                        <label for="city" >City Filter </label><br />
                        <select id="city" name="city">
                            <option value="none">Select City</option>
                            {
                                data.map((value, index) => {
                                    return (
                                        <option key={index} value={value.city}>
                                            {value.city}
                                        </option>
                                    )
                                })
                            }
                        </select>
                        <br />
                        <div className="patients">
                            <span>
                                Total patients: {this.patients(data)}
                            </span>
                        </div>
                        <section className="card chart-bottom">
                            <div className="card-body chart-body ">
                                <Chart data={data} axes={axes2} tooltip />
                            </div>
                        </section>
                    </div> */}
                </div>
                <div className="row">
                    <div className="col-12 graph-wrapper ">
                        <h3>2. Social distancing - all locations</h3>
                        <label for="city" >City Filter </label><br />
                        <select className="selectCity" id="city" name="city">
                            <option value="none">Select City</option>
                            {
                                data.map((value, index) => {
                                    return (
                                        <option key={index} value={value.city}>
                                            {value.city}
                                        </option>
                                    )
                                })
                            }
                        </select>
                        <DateRangePicker
                            onChange={this.handleSelect}
                            value={value}
                        />
                        <br />
                        {/* <div className="patients">
                            <span>
                                Total patients: {this.patients(data)}
                            </span>
                        </div> */}
                        <section className="card chart-bottom">
                            <div className="card-body chart-body ">
                            <Chart
                                chartType="LineChart"
                                loader={<div>Loading Chart</div>}
                                data={[
                                    ['x', 'People'],
                                    [new Date( 2020, 0, 1), 5],  [new Date( 2020, 0, 2), 7],  [new Date( 2020, 0, 3), 3],
                                    [new Date( 2020, 0, 4), 1],  [new Date( 2020, 0, 5), 3],  [new Date( 2020, 0, 6), 4],
                                    [new Date( 2020, 0, 7), 3],  [new Date( 2020, 0, 8), 4],  [new Date( 2020, 0, 9), 2],
                                    [new Date( 2020, 0, 10), 5], [new Date( 2020, 0, 11), 8], [new Date( 2020, 0, 12), 6],
                                    [new Date( 2020, 0, 13), 3], [new Date( 2020, 0, 14), 3], [new Date( 2020, 0, 15), 5],
                                    [new Date( 2020, 0, 16), 7], [new Date( 2020, 0, 17), 6], [new Date( 2020, 0, 18), 6],
                                    [new Date( 2020, 0, 19), 3], [new Date( 2020, 0, 20), 1], [new Date( 2020, 0, 21), 2],
                                    [new Date( 2020, 0, 22), 4], [new Date( 2020, 0, 23), 6], [new Date( 2020, 0, 24), 5],
                                    [new Date( 2020, 0, 25), 9], [new Date( 2020, 0, 26), 4], [new Date( 2020, 0, 27), 9],
                                    [new Date( 2020, 0, 28), 8], [new Date( 2020, 0, 29), 6], [new Date( 2020, 0, 30), 4],
                                    [new Date( 2020, 0, 31), 6], [new Date( 2020, 1, 1), 7],  [new Date( 2020, 1, 2), 9]
                                ]}
                                options={{
                                    title: 'No. of people not maintaining social distance on a given date',
                                    backgroundColor: 'transparent',
                                    width: '100%',
                                    height: '100%',
                                    hAxis: {
                                      format: 'M/d/yy',
                                      gridlines: {count: 15}
                                    },
                                    vAxis: {
                                      gridlines: {color: 'none'},
                                      minValue: 0
                                    }
                                  }}
                                rootProps={{ 'data-testid': '1' }}
                            />
                            </div>
                        </section>
                    </div>
                    {/* <div className="col-xl-6 col-lg-3 col-md-4 col-sm-4 graph-wrapper ">
                        <h3>1. Social distancing - Filtered by locations</h3>
                        <label for="city" >City Filter </label><br />
                        <select id="city" name="city">
                            <option value="none">Select City</option>
                            {
                                data.map((value, index) => {
                                    return (
                                        <option key={index} value={value.city}>
                                            {value.city}
                                        </option>
                                    )
                                })
                            }
                        </select>
                        <br />
                        <div className="patients">
                            <span>
                                Total patients: {this.patients(data)}
                            </span>
                        </div>
                        <section className="card chart-bottom">
                            <div className="card-body chart-body ">
                                <Chart data={data} axes={axes} tooltip />
                            </div>
                        </section>
                    </div> */}
                </div>
                <div className="row">
                    <div className="col-12 graph-wrapper ">
                        <h3>3. Temperature Detection - all locations</h3>
                        <label for="city" >City Filter </label><br />
                        <select className="selectCity" id="city" name="city">
                            <option value="none">Select City</option>
                            {
                                data.map((value, index) => {
                                    return (
                                        <option key={index} value={value.city}>
                                            {value.city}
                                        </option>
                                    )
                                })
                            }
                        </select>
                        <DateRangePicker
                            onChange={this.handleSelect}
                            value={value}
                        />
                        <br />
                        {/* <div className="patients">
                            <span>
                                Total patients: {this.patients(data)}
                            </span>
                        </div> */}
                        <section className="card chart-bottom">
                            <div className="card-body chart-body ">
                            <Chart
                                chartType="LineChart"
                                loader={<div>Loading Chart</div>}
                                data={[
                                    ['x', 'People'],
                                    [new Date( 2020, 0, 1), 5],  [new Date( 2020, 0, 2), 7],  [new Date( 2020, 0, 3), 3],
                                    [new Date( 2020, 0, 4), 1],  [new Date( 2020, 0, 5), 3],  [new Date( 2020, 0, 6), 4],
                                    [new Date( 2020, 0, 7), 3],  [new Date( 2020, 0, 8), 4],  [new Date( 2020, 0, 9), 2],
                                    [new Date( 2020, 0, 10), 5], [new Date( 2020, 0, 11), 8], [new Date( 2020, 0, 12), 6],
                                    [new Date( 2020, 0, 13), 3], [new Date( 2020, 0, 14), 3], [new Date( 2020, 0, 15), 5],
                                    [new Date( 2020, 0, 16), 7], [new Date( 2020, 0, 17), 6], [new Date( 2020, 0, 18), 6],
                                    [new Date( 2020, 0, 19), 3], [new Date( 2020, 0, 20), 1], [new Date( 2020, 0, 21), 2],
                                    [new Date( 2020, 0, 22), 4], [new Date( 2020, 0, 23), 6], [new Date( 2020, 0, 24), 5],
                                    [new Date( 2020, 0, 25), 9], [new Date( 2020, 0, 26), 4], [new Date( 2020, 0, 27), 9],
                                    [new Date( 2020, 0, 28), 8], [new Date( 2020, 0, 29), 6], [new Date( 2020, 0, 30), 4],
                                    [new Date( 2020, 0, 31), 6], [new Date( 2020, 1, 1), 7],  [new Date( 2020, 1, 2), 9]
                                ]}
                                options={{
                                    title: 'No. of people having high temperature on a given date',
                                    backgroundColor: 'transparent',
                                    width: '100%',
                                    height: '100%',
                                    hAxis: {
                                      format: 'M/d/yy',
                                      gridlines: {count: 15}
                                    },
                                    vAxis: {
                                      gridlines: {color: 'none'},
                                      minValue: 0
                                    }
                                  }}
                                rootProps={{ 'data-testid': '1' }}
                            />
                            </div>
                        </section>
                    </div>
                    {/* <div className="col-xl-6 col-lg-3 col-md-4 col-sm-4 graph-wrapper ">
                        <h3>2. Temperature Detection - Filter by locations</h3>
                        <label for="city" >City Filter </label><br />
                        <select id="city" name="city">
                            <option value="none">Select City</option>
                            {
                                data.map((value, index) => {
                                    return (
                                        <option key={index} value={value.city}>
                                            {value.city}
                                        </option>
                                    )
                                })
                            }
                        </select>
                        <br />
                        <div className="patients">
                            <span>
                                Total patients: {this.patients(data)}
                            </span>
                        </div>
                        <section className="card chart-bottom">
                            <div className="card-body chart-body ">
                                <Chart data={data} axes={axes} tooltip />
                            </div>
                        </section>
                    </div> */}
                </div>
                {/* </div> */}
            </section>
        )
    }
}


const mapStateToProps = (state) => {
    return {
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
    }
}
export default connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(Matrics)
    ;