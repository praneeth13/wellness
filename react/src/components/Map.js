import React, { Component } from 'react';
import L from 'leaflet'; // Same as `import * as L from 'leaflet'` with webpack

import markershadow from '../assets/images/marker-shadow.png';
import cameramarker from '../assets/images/camera.jpg';
import cameratickmarker from '../assets/images/cameratick.jpg';
import sensormarker from '../assets/images/sensor.png';
import sensortickmarker from '../assets/images/sensortick.png';
import telescopemarker from '../assets/images/telescope.png';
import telescopetickmarker from '../assets/images/telescopetick.png';
import thermalmarker from '../assets/images/thermalcam.jpg';
import thermaltickmarker from '../assets/images/thermalcam-tick.jpg';

import '../assets/css/map.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import "react-toggle/style.css"
import * as request from '../actions/map.action';

const cameraIcon = L.icon({
  iconUrl: cameramarker,
  shadowUrl: markershadow,
  iconSize: [30, 25], // size of the icon
  shadowSize: [41, 41], // size of the shadow
  iconAnchor: [12, 41], // point of the icon which will correspond to marker's location
  shadowAnchor: [12, 41],  // the same for the shadow
  popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});


const cameratickIcon = L.icon({
  iconUrl: cameratickmarker,
  shadowUrl: markershadow,
  iconSize: [30, 25], // size of the icon
  shadowSize: [41, 41], // size of the shadow
  iconAnchor: [12, 41], // point of the icon which will correspond to marker's location
  shadowAnchor: [12, 41],  // the same for the shadow
  popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});

const thermalicon = L.icon({
  iconUrl: thermalmarker,
  shadowUrl: markershadow,
  iconSize: [30, 25], // size of the icon
  shadowSize: [41, 41], // size of the shadow
  iconAnchor: [12, 41], // point of the icon which will correspond to marker's location
  shadowAnchor: [12, 41],  // the same for the shadow
  popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});

const thermaltickicon = L.icon({
  iconUrl: thermaltickmarker,
  shadowUrl: markershadow,
  iconSize: [30, 25], // size of the icon
  shadowSize: [41, 41], // size of the shadow
  iconAnchor: [12, 41], // point of the icon which will correspond to marker's location
  shadowAnchor: [12, 41],  // the same for the shadow
  popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});

const sensorIcon = L.icon({
  iconUrl: sensormarker,
  shadowUrl: markershadow,

  iconSize: [30, 30], // size of the icon
  shadowSize: [41, 41], // size of the shadow
  iconAnchor: [12, 41], // point of the icon which will correspond to marker's location
  shadowAnchor: [12, 41],  // the same for the shadow
  popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});

const sensortickIcon = L.icon({
  iconUrl: sensortickmarker,
  shadowUrl: markershadow,

  iconSize: [30, 30], // size of the icon
  shadowSize: [41, 41], // size of the shadow
  iconAnchor: [12, 41], // point of the icon which will correspond to marker's location
  shadowAnchor: [12, 41],  // the same for the shadow
  popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});

const telescopeIcon = L.icon({
  iconUrl: telescopemarker,
  shadowUrl: markershadow,

  iconSize: [30, 30], // size of the icon
  shadowSize: [41, 41], // size of the shadow
  iconAnchor: [12, 41], // point of the icon which will correspond to marker's location
  shadowAnchor: [12, 41],  // the same for the shadow
  popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});

const telescopetickIcon = L.icon({
  iconUrl: telescopetickmarker,
  shadowUrl: markershadow,

  iconSize: [30, 30], // size of the icon
  shadowSize: [41, 41], // size of the shadow
  iconAnchor: [12, 41], // point of the icon which will correspond to marker's location
  shadowAnchor: [12, 41],  // the same for the shadow
  popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});


class Map extends Component {

  constructor(props) {
    super(props);

    this.state = {
      showSigned: true,
      showSpoken: true,
      showWritten: true,
      showUnknown: true,
      ipaccess: true,
      feed: false,
      camList: ['Denver','New York','Virginia','Washington','Alabama', 'California', ' Illinois', 'Texas', 'Arizona', 'Pennsylvania', 'Indiana', 'Illinois', 'Florida','California', 'Kentucky', 'Arizona', 'California', 'Ohio', 'South Carolina',]
    };
  }

  dist_map = 0
  tempID = 0
  markers = []
  selectedCamera = []
  selectedIcon = (iconselected) => {
    this.tempID = this.tempID + 1
    let pid = 0
    let icon = 0
    if (iconselected === 1) {
      icon = cameraIcon
      pid = "c"
    }
    if (iconselected === 2) {
      icon = telescopeIcon
      pid = "t"
    }
    if (iconselected === 3) {
      icon = sensorIcon
      pid = "s"
    }
    if (iconselected === 4) {
      icon = thermalicon
      pid = "th"
    }
    this.plotMarkers(pid, 0, icon)

  }

  selectedMarkers = (evt, point) => {
    var index = this.selectedCamera.indexOf(evt.options.pid)
    console.log(evt.options.pid)
    let markerType = evt.options.pid.split("_")[0]
    if (index !== -1) {
      this.selectedCamera.splice(index, 1);
      //back to normal icon
      if (markerType === "c") {
        point.setIcon(cameraIcon)
      }
      if (markerType === "t") {
        point.setIcon(telescopeIcon)
      }
      if (markerType === "s") {
        point.setIcon(sensorIcon)
      }
      if (markerType === "th") {
        point.setIcon(thermalicon)
      }
    }
    else {
      this.selectedCamera.push(evt.options.pid)
      if (markerType === "c") {
        point.setIcon(cameratickIcon)
      }
      if (markerType === "t") {
        point.setIcon(telescopetickIcon)
      }
      if (markerType === "s") {
        point.setIcon(sensortickIcon)
      }
      if (markerType === "th") {
        point.setIcon(thermaltickicon)
      }

    }

    if (this.selectedCamera.length == 0) {
      this.setState({ feed: false })
    }
    else {
      this.setState({ feed: true })
    }
  }

  viewFeed() {
    if (this.selectedCamera.length > 2) {
      alert("Please select 2 cameras at a time !!")
      return
    }
    if (this.selectedCamera.length == 0) {
      alert("Please select altleast 1 camera to view feed !!")
      return
    }

    this.props.history.push('/viewfeed/')
  }

  plotMarkers(points, fromwhere, icon) {
    let pid = 0
    if (fromwhere === 1) {
      this.tempID = points.length
      for (var i = 0; i < points.length; i++) {
        let data = points[i]

        if (data.type === 1) {
          icon = cameraIcon
          pid = "c"
        }
        if (data.type === 2) {
          icon = telescopeIcon
          pid = "t"
        }
        if (data.type === 3) {
          icon = sensorIcon
          pid = "s"
        }

        pid = pid + "_" + data.id
        // Loading already saved points into Map
        this.loadMap(pid, icon, data.lat, data.long)
      }
    }
    else {
      pid = points
      pid = pid + "_" + this.tempID
      // Loading new point Temp point
      this.loadMap(pid, icon, this.dist_map.getCenter().lat, this.dist_map.getCenter().lng)
      let tempObj = {}
      tempObj.lat = this.dist_map.getCenter().lat
      tempObj.long = this.dist_map.getCenter().lng
      tempObj.type = pid.split("_")[0]

      this.markers.push(tempObj);
    }


  }

  loadMap(pid, icon, lat, long) {
    var options = {
      pid: pid,
      icon: icon,
      draggable: true
    };

    let self = this
    var point = L.marker([lat, long], options).addTo(this.dist_map);
    point.on('dragend', function (evt) {
      var marker = evt.target;
      var position = marker.getLatLng();
      point.setLatLng(position, { draggable: 'true' }).update();
    });

    point.on('click', function (evt) {
      self.selectedMarkers(evt.target, point)
    })


  }

  handleChange() {
    this.setState({
      ipaccess: !this.state.ipaccess,
    });
    let { ipaccess } = { ...this.state }

  }

  componentDidMount() {

    request.getAllPoints().then(response => {
      console.log(response)
      if (response && response.status == 200) {

        this.dist_map = L.map('mapid').setView([response.lat, response.long], 9);
        L.tileLayer(
          'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}',
          {
            maxZoom: 10,
            draggable: true,
            id: 'mapbox.streets',
            accessToken:
              'pk.eyJ1IjoicHJvdG9jb2w3IiwiYSI6ImNrMjUxZjEzYTAwbXYzbHRscnE5dDFnOWQifQ.uOVge_4xINbcWwQ8gA-rGg',
          }
        ).addTo(this.dist_map);
        this.markers = response.data
        this.setState({ points: response.data })
        //Add Markers
        this.plotMarkers(response.data, 1, null)
      }
      else {
        console.log("Server Error")
      }
    });
   
  }

  render() {
    // function displayLocation(latitude, longitude) {

    // };
    // displayLocation(13.082680,80.270721)
    // request.getPlaceName(13.082680,80.270721).then(res=>{
    //   console.log(res)
    // })
    return (
      <section role="main" className="content-body">

        <header className={'page-header ' + (this.props.showMenu ? '' : 'header-left')}>
          <h2>Map</h2>
        </header>


        <div className="row">
          <div className="ml-auto pull-right mr-14">
            <div id="box">Place on the map :
          <span class="poi-type" data-toggle="tooltip" title="Camera"><img class="drag" type="tree" src={cameramarker} alt="" onClick={e => { this.selectedIcon(1) }} /></span>
              <span class="poi-type" data-toggle="tooltip" title="telescope"><img class="drag" type="red" src={telescopemarker} alt="" onClick={e => { this.selectedIcon(2) }} /></span>
              <span class="poi-type" data-toggle="tooltip" title="Thermal Camera"><img class="drag" type="red" src={thermalmarker} alt="" onClick={e => { this.selectedIcon(4) }} /></span>
              <span class="poi-type " data-toggle="tooltip" title=""><img class="drag" type="black" src={sensormarker} alt="" onClick={e => { this.selectedIcon(3) }} /></span>
            </div>
            {this.state.feed === true ?
              <button className="btn btn-success" onClick={e => { this.viewFeed() }}>View Feed</button>
              : null
            }
          </div>
        </div>
        <div id="mapid" className="mt-3 mb-3" />
        <div className="row  pl-10 ">
          <div className="card lat-long-card">
            <div className="card-header">
              <label className="  mt-2">Cam list</label>
              <input className="form-control input-sm d-inline float-right w-10" placeholder="Search place name"></input>
            </div>
            <div className="card-body row" >

              {this.state.camList && this.state.camList.map((cam, index) =>
                <ul className="li-none col-3">
                  <li className="c-pointer">
                    <span className="poi-type " data-toggle="tooltip" title="Thermal Camera"><img className="drag" width={40} height={40} type="red" src={index%2===0?thermalmarker:cameramarker} alt="" onClick={e => { this.selectedIcon(4) }} /> {cam}</span>
                  </li>
                </ul>
              )}
            </div>
          </div>
        </div>
      </section>



    );
  }
}

export default Map;
