import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'rc-datepicker/lib/style.css';
import { DatePickerInput } from 'rc-datepicker';

class Events extends Component {
    constructor(props){
        super(props)
        this.state = {
            date: null,
            array: [],
            showMenu: false
        }
    }

    getRandomCams = () => {
        let array = []
        let i = 0 
        let rand = Math.floor((Math.random() * 10) + 1);
        for(i = 0 ; i < rand ; i++)
            array.push(1)
        this.setState({array})
    }

    render(){
        let array = this.state.array
        return(
            <section role="main" className="content-body">
                <header className={'page-header ' + (this.state.showMenu ? '' : 'header-left')}>
                    <h2>Events</h2>
                </header>
                <h3>Events</h3>
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">Choose Date</h5><br />
                                <div>
                                    <DatePickerInput
                                    type="button"
                                    value={this.state.date ? new Date(this.state.date) : new Date()}
                                    className='my-custom-datepicker-component'
                                    />
                                </div><br />
                                <button className="btn btn-primary" onClick={this.getRandomCams}>Search events</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row" >
                    {array.map((value, i) =>
                        <div className="col-4">
                            <label className="">Cam {i + 1}</label><br />
                            <img className="" src={"https://articulate-heroes.s3.amazonaws.com/uploads/rte/lovkahws_2018-10-31_11-25-18.jpg"} 
                                width="300" height="300" alt=""></img>
                        </div>
                    )}
                </div>
            </section>
        )
    }
}

export default Events
