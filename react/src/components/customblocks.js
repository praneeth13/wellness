export const addCustomBlocks = (Blockly) => {

    Blockly.Blocks['send_email'] = {
        init: function() {
            this.appendValueInput("To Email")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("To Email");
            this.appendValueInput("Subject")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Subject");
            this.appendValueInput("Body")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Body");
          this.setPreviousStatement(true, null);
          this.setNextStatement(true, null);
          this.setColour(0);
          this.setTooltip("");
          this.setHelpUrl("");
        }
    }

    Blockly.Blocks['webservice_call'] = {
        init: function() {
            this.appendDummyInput()
                .appendField("Call Web Services");
            this.setPreviousStatement(true, null);
            this.setNextStatement(true, null);
            this.setColour(0);
            this.setTooltip("");
            this.setHelpUrl("");
        }
      };

    Blockly.Blocks['trigger_led'] = {
        init: function() {
            this.appendDummyInput()
                .appendField("Trigger")
                .appendField(new Blockly.FieldDropdown([["LED 1","led1"], ["LED 2","led2"]]), "led");
            this.appendDummyInput()
                .appendField("Switch")
                .appendField(new Blockly.FieldDropdown([["on","on"], ["off","off"]]), "on_off");
            this.appendDummyInput()
                .appendField("Color")
                .appendField(new Blockly.FieldDropdown([["red","red"], ["green","green"], ["yellow","yellow"]]), "colors");
            this.setPreviousStatement(true, null);
            this.setNextStatement(true, null);
            this.setColour(0);
            this.setTooltip("");
            this.setHelpUrl("");
        }
      };

    Blockly.Blocks['send_text'] = {
        init: function() {
          this.appendDummyInput()
              .appendField("Send Text")
              .appendField(new Blockly.FieldTextInput("enter your text"), "text");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
          this.setColour(0);
       this.setTooltip("");
       this.setHelpUrl("");
        }
      };

    Blockly.Blocks['var_socialDistancing'] = {
        init: function() {
          this.appendDummyInput()
              .appendField("Social Distancing");
          this.setOutput(true, null);
       this.setTooltip("");
       this.setHelpUrl("");
       this.setColour(120);
        }
      };
      
    Blockly.Blocks['var_temperature'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("Temperature");
        this.setOutput(true, null);
    this.setTooltip("");
    this.setHelpUrl("");
    this.setColour(120);
    }
    };

    Blockly.Blocks['var_faceCovering'] = {
        init: function() {
          this.appendDummyInput()
              .appendField("Face Coverings");
          this.setOutput(true, null);
       this.setTooltip("");
       this.setHelpUrl("");
       this.setColour(120);
        }
    };

    Blockly.Blocks['var_facialRecognition'] = {
        init: function() {
          this.appendDummyInput()
              .appendField("Facial Recognition");
          this.setOutput(true, null);
       this.setTooltip("");
       this.setHelpUrl("");
       this.setColour(120);
        }
    };

    Blockly.Blocks['run_inference'] = {
        init: function() {
          this.appendDummyInput()
              .appendField("Run Inference")
              .setAlign(Blockly.ALIGN_CENTRE);
          this.appendValueInput("NAME")
              .setCheck(null)
              .appendField("Model Name")
              .setAlign(Blockly.ALIGN_RIGHT);
          this.setColour(230);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(0);
       this.setTooltip("");
       this.setHelpUrl("");
        }
      };
    
    Blockly.Blocks['gp_createmask'] = {
        init: function() {
            this.appendDummyInput()
                .appendField("Create Mask")
                .setAlign(Blockly.ALIGN_CENTRE);
            this.appendDummyInput()
                .appendField("Backbone")
                .appendField(new Blockly.FieldTextInput("enter value"), "backdone");
            this.appendDummyInput()
                .appendField("Type")
                .appendField(new Blockly.FieldDropdown([["Instance Segmentation","Instance Segmentation"], ["Semantic Segmentation ","Semantic Segmentation"]]), "Type");
            this.appendDummyInput()
                .appendField("General Overlays")
                .appendField(new Blockly.FieldDropdown([["true","True"], ["false","False"]]), "generalOverlays");
            this.appendValueInput("NAME")
                .setCheck("")
                .setAlign(Blockly.ALIGN_RIGHT)
                .appendField("Model");
            this.setPreviousStatement(true, null);
            this.setNextStatement(true, null);
            this.setColour(0);
        this.setTooltip("");
        this.setHelpUrl("");
        }
    };
    
    Blockly.Blocks['gp_dataoutput'] = {
        init: function() {
          this.appendDummyInput()
              .appendField("Data Output")
              .setAlign(Blockly.ALIGN_CENTRE);
          this.appendDummyInput()
              .appendField("Type")
              .appendField(new Blockly.FieldDropdown([["File","File"], ["Database Insert","Database Insert"], ["Web Service Call","Web Service Call"]]), "type");
          this.appendDummyInput()
              .appendField("Output Content")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField(new Blockly.FieldTextInput("Enter Value"), "NAME");
          this.setPreviousStatement(true, null);
          this.setNextStatement(true, null);
          this.setColour(0);
         this.setTooltip("");
         this.setHelpUrl("");
        }
    };
      
      Blockly.Blocks['sendemail'] = {
        init: function() {
          this.appendDummyInput()
              .appendField("Send Email")
              .setAlign(Blockly.ALIGN_CENTRE);
          this.appendValueInput("To List")
              .setCheck("Array")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("To List");
          this.appendValueInput("CC List")
              .setCheck("Array")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("CC List");
          this.appendValueInput("BCC List")
              .setCheck("Array")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("BCC List");
          this.appendValueInput("Subject")
              .setCheck("String")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("Subject");
          this.appendValueInput("Body")
              .setCheck("String")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("Body");
          this.setPreviousStatement(true, null);
          this.setNextStatement(true, null);
          this.setColour(0);
          this.setTooltip("");
          this.setHelpUrl("");
        }
    };

    Blockly.Blocks['database_query'] = {
        init: function() {
          this.appendDummyInput()
              .appendField("Database Query")
              .setAlign(Blockly.ALIGN_CENTRE);
          this.appendDummyInput()
              .appendField("Database Type")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField(new Blockly.FieldDropdown([["MySQL","MySQL"], ["MongoDB","MongoDB"]]), "Select Database Type");
          this.appendValueInput("Server")
              .setCheck("String")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("Server URL");
          this.appendValueInput("Username")
              .setCheck("String")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("Username");
          this.appendValueInput("Password")
              .setCheck("String")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("Password");
          this.appendValueInput("Database Instance Name")
              .setCheck("String")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("Database Instance Name");
          this.appendValueInput("SQL Query")
              .setCheck("String")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("SQL Query");
          this.setPreviousStatement(true, null);
          this.setNextStatement(true, null);
          this.setColour(0);
       this.setTooltip("");
       this.setHelpUrl("");
        }
      };

      Blockly.Blocks['database_insert'] = {
        init: function() {
          this.appendDummyInput()
              .appendField("Database Insert")
              .setAlign(Blockly.ALIGN_CENTRE);
          this.appendDummyInput()
              .appendField("Database Type")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField(new Blockly.FieldDropdown([["MySQL","MySQL"], ["MongoDB","MongoDB"]]), "Select Database Type");
          this.appendValueInput("Server")
              .setCheck("String")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("Server URL");
          this.appendValueInput("Username")
              .setCheck("String")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("Username");
          this.appendValueInput("Password")
              .setCheck("String")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("Password");
          this.appendValueInput("Database Instance Name")
              .setCheck("String")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("Database Instance Name");
          this.appendValueInput("SQL Query")
              .setCheck("String")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("SQL Query");
          this.setPreviousStatement(true, null);
          this.setNextStatement(true, null);
          this.setColour(0);
       this.setTooltip("");
       this.setHelpUrl("");
        }
      };

      Blockly.Blocks['open_file'] = {
        init: function() {
          this.appendDummyInput()
              .appendField("Open File")
              .setAlign(Blockly.ALIGN_CENTRE);
          this.appendDummyInput()
              .appendField("File Type")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField(new Blockly.FieldDropdown([["text","'text'"], ["CSV","'CSV'"]]), "File Type");
          this.appendValueInput("Filename")
              .setCheck("String")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("Filename");
          this.appendDummyInput()
              .appendField("*File Required")
              .setAlign(Blockly.ALIGN_CENTRE);
          this.setPreviousStatement(true, null);
          this.setNextStatement(true, null);
          this.setColour(0);
       this.setTooltip("");
       this.setHelpUrl("");
        }
      };
      

    Blockly.Blocks['write_to_file'] = {
        init: function() {
            this.appendDummyInput()
                .appendField("Write to File")
                .setAlign(Blockly.ALIGN_CENTRE);
            this.appendDummyInput()
                .appendField("File Type")
                .setAlign(Blockly.ALIGN_RIGHT)
                .appendField(new Blockly.FieldDropdown([["text","text"], ["CSV","CSV"]]), "File Type");
            this.appendValueInput("Path to File")
                .setCheck("String")
                .setAlign(Blockly.ALIGN_RIGHT)
                .appendField("File Name");
            this.appendValueInput("Value")
                .setCheck("String")
                .setAlign(Blockly.ALIGN_RIGHT)
                .appendField("Input Value");
            this.setPreviousStatement(true, null);
            this.setNextStatement(true, null);
            this.setColour(0);
        this.setTooltip("");
        this.setHelpUrl("");
        }
    };

    Blockly.Blocks['loop_file'] = {
        init: function() {
          this.appendDummyInput()
              .appendField("Loop File")
              .setAlign(Blockly.ALIGN_CENTRE);
          this.appendDummyInput()
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("Header?")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField(new Blockly.FieldDropdown([["True","True"], ["False","False"]]), "File has Header");
          this.appendValueInput("File Path")
              .setCheck("String")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("File Path");
          this.appendValueInput("Delimiter")
              .setCheck("String")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("Delimiter");
          this.appendValueInput("Assign To")
              .setCheck("String")
              .setAlign(Blockly.ALIGN_RIGHT)
              .appendField("Assign To");
          this.appendDummyInput()
              .appendField("*File Required")
              .setAlign(Blockly.ALIGN_CENTRE);
          this.setPreviousStatement(true, null);
          this.setNextStatement(true, null);
          this.setColour(0);
       this.setTooltip("");
       this.setHelpUrl("");
        }
      };

    Blockly.Blocks['loop_data'] = {
    init: function() {
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField("Loop Data");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Database Type")
            .appendField(new Blockly.FieldDropdown([["MySQL","MySQL"], ["MongoDB","MongoDB"]]), "Database Type");
        this.appendValueInput("Server ")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Server ");
        this.appendValueInput("Username")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("username");
        this.appendValueInput("Password")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Password");
        this.appendValueInput("Database Instance Name")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Database Instance Name");
        this.appendValueInput("SQL Query")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("SQL Query");
        this.appendValueInput("Port")
            .setCheck("Number")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Port");
        this.appendValueInput("Assign To")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Assign To");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(0);
    this.setTooltip("");
    this.setHelpUrl("");
    }
      };

    Blockly.Blocks['execute_script'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("Execute Python Script")
            .setAlign(Blockly.ALIGN_RIGHT);
        this.appendValueInput("Script name")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Script name");
        this.appendValueInput("Path to script")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Path to script");
        this.appendDummyInput()
            .appendField("Generate Output Log")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(new Blockly.FieldDropdown([["true","true"], ["false","false"]]), "generateLog");
        this.appendValueInput("Log File name")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Log File name");
        this.appendValueInput("Path to Log File")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Path to Log File");
        // this.appendDummyInput()
        //     .appendField("Log file type")
        //     .setAlign(Blockly.ALIGN_RIGHT)
        //     .appendField(new Blockly.FieldDropdown([["NCSA","NCSA"], ["W3C Extended","W3C Extended"], ["FTP Logs","FTP Logs"]]), "logFileType");
        this.setColour(230);
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(0);
    this.setTooltip("");
    this.setHelpUrl("");
    }
    };


    Blockly.Blocks['ai_2dconvnet'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("2D ConvNet");
        this.setOutput(true, null);
        this.setColour(270);
    this.setTooltip("");
    this.setHelpUrl("");
    }
    };
    
    Blockly.Blocks['ai_3dconvnet'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("3D ConvNet");
        this.setOutput(true, null);
        this.setColour(270);
    this.setTooltip("");
    this.setHelpUrl("");
    }
    };
    
    Blockly.Blocks['ai_rnn'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("RNN");
        this.setOutput(true, null);
        this.setColour(270);
    this.setTooltip("");
    this.setHelpUrl("");
    }
    };
    
    Blockly.Blocks['ai_rnnwithlstm'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("RNN with LSTM");
        this.setOutput(true, null);
        this.setColour(270);
    this.setTooltip("");
    this.setHelpUrl("");
    }
    };

    Blockly.Blocks['input_string'] = {
        init: function() {
          this.appendDummyInput()
              .appendField("\"")
              .appendField(new Blockly.FieldTextInput(""), "string")
              .appendField("\"");
          this.setOutput(true, null);
          this.setColour(210);
       this.setTooltip("");
       this.setHelpUrl("");
        }
    };

    Blockly.Python['run_inference'] = function(block) {
        var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
        // TODO: Assemble Python into code variable.
        var code = 'obj.run_inference({"model":'+value_name+'})\n';
        return code;
      };

    Blockly.Python['gp_createmask'] = function(block) {
        var text_backbone = block.getFieldValue('backbone');
        var dropdown_type = block.getFieldValue('Type');
        var dropdown_generaloverlays = block.getFieldValue('generalOverlays');
        var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
        // TODO: Assemble Python into code variable.
        var code = 'obj.create_mask({"backbone" : "'+text_backbone+'", "type" : "'+dropdown_type+'", "generalOverlays" : "'+dropdown_generaloverlays+'", "model" : "'+value_name+'"})\n';
        return code;
    };

    Blockly.Python['sendemail'] = function(block) {
        var to_list = Blockly.Python.valueToCode(block, 'To List', Blockly.Python.ORDER_ATOMIC);
        var cc_list = Blockly.Python.valueToCode(block, 'CC List', Blockly.Python.ORDER_ATOMIC);
        var bcc_list = Blockly.Python.valueToCode(block, 'BCC List', Blockly.Python.ORDER_ATOMIC);
        var subject = Blockly.Python.valueToCode(block, 'Subject', Blockly.Python.ORDER_ATOMIC);
        var body = Blockly.Python.valueToCode(block, 'Body', Blockly.Python.ORDER_ATOMIC);
        var code = 'obj.send_email({"To List" : "'+to_list+'", "CC List" : "'+cc_list+'", "BCC List" : "'+bcc_list+'", "Subject" : "'+subject+'", "Body" : "'+body+'"})\n';
        return code;
    };

    Blockly.Python['database_query'] = function(block) {
        var dropdown_select_database_type = block.getFieldValue('Select Database Type');
        var value_server = Blockly.Python.valueToCode(block, 'Server', Blockly.Python.ORDER_ATOMIC);
        var value_username = Blockly.Python.valueToCode(block, 'Username', Blockly.Python.ORDER_ATOMIC);
        var value_password = Blockly.Python.valueToCode(block, 'Password', Blockly.Python.ORDER_ATOMIC);
        var value_database_instance_name = Blockly.Python.valueToCode(block, 'Database Instance Name', Blockly.Python.ORDER_ATOMIC);
        var value_sql_query = Blockly.Python.valueToCode(block, 'SQL Query', Blockly.Python.ORDER_ATOMIC);
        // TODO: Assemble Python into code variable.
        var code = 'obj.database_query({"Database Type":"'+dropdown_select_database_type+'", "Server":"'+value_server+'", "Username":"'+value_username+'", "Password":"'+value_password+'", "Database Instance Name":"'+value_database_instance_name+'", "SQL Query":"'+value_sql_query+'"})\n';
        return code;
      };

    Blockly.Python['database_insert'] = function(block) {
        var dropdown_select_database_type = block.getFieldValue('Select Database Type');
        var value_server = Blockly.Python.valueToCode(block, 'Server', Blockly.Python.ORDER_ATOMIC);
        var value_username = Blockly.Python.valueToCode(block, 'Username', Blockly.Python.ORDER_ATOMIC);
        var value_password = Blockly.Python.valueToCode(block, 'Password', Blockly.Python.ORDER_ATOMIC);
        var value_database_instance_name = Blockly.Python.valueToCode(block, 'Database Instance Name', Blockly.Python.ORDER_ATOMIC);
        var value_sql_query = Blockly.Python.valueToCode(block, 'SQL Query', Blockly.Python.ORDER_ATOMIC);
        // TODO: Assemble Python into code variable.
        var code = 'obj.database_query({"Database Type":"'+dropdown_select_database_type+'", "Server":"'+value_server+'", "Username":"'+value_username+'", "Password":"'+value_password+'", "Database Instance Name":"'+value_database_instance_name+'", "SQL Query":"'+value_sql_query+'"})\n';
        return code;
    };

    Blockly.Python['open_file'] = function(block) {
        var dropdown_file_type = block.getFieldValue('File Type');
        var value_path_to_file = Blockly.Python.valueToCode(block, 'Filename', Blockly.Python.ORDER_ATOMIC);
        // TODO: Assemble Python into code variable.
        var code = 'obj.open_file({"File Path":'+value_path_to_file+', "File Type":'+dropdown_file_type+'})\n';
        return code;
    };

    Blockly.Python['write_to_file'] = function(block) {
        var dropdown_file_type = block.getFieldValue('File Type');
        var value_path_to_file = Blockly.Python.valueToCode(block, 'Path to File', Blockly.Python.ORDER_ATOMIC);
        var value_value = Blockly.Python.valueToCode(block, 'Value', Blockly.Python.ORDER_ATOMIC);
        // TODO: Assemble Python into code variable.
        var code = 'obj.write_to_file({ "Path":"'+value_path_to_file+'", "Value":"'+value_value+'", "File Type":"'+dropdown_file_type+'"})\n';
        return code;
    };

    Blockly.Python['loop_file'] = function(block) {
        var dropdown_file_has_header = block.getFieldValue('File has Header');
        var value_file_path = Blockly.Python.valueToCode(block, 'File Path', Blockly.Python.ORDER_ATOMIC);
        var value_delimiter = Blockly.Python.valueToCode(block, 'Delimiter', Blockly.Python.ORDER_ATOMIC);
        var value_assign_to = Blockly.Python.valueToCode(block, 'Assign To', Blockly.Python.ORDER_ATOMIC);
        // TODO: Assemble Python into code variable.
        var code = 'obj.loop_file({"File Path":'+value_file_path+', "Demiliter":'+value_delimiter+', "Assigned To":'+value_assign_to+', "File has Header":'+dropdown_file_has_header+'})\n';
        return code;
    };

    Blockly.Python['loop_data'] = function(block) {
        var dropdown_database_type = block.getFieldValue('Database Type');
        var value_server = Blockly.Python.valueToCode(block, 'Server ', Blockly.Python.ORDER_ATOMIC);
        var value_username = Blockly.Python.valueToCode(block, 'Username', Blockly.Python.ORDER_ATOMIC);
        var value_password = Blockly.Python.valueToCode(block, 'Password', Blockly.Python.ORDER_ATOMIC);
        var value_database_instance_name = Blockly.Python.valueToCode(block, 'Database Instance Name', Blockly.Python.ORDER_ATOMIC);
        var value_sql_query = Blockly.Python.valueToCode(block, 'SQL Query', Blockly.Python.ORDER_ATOMIC);
        var value_port = Blockly.Python.valueToCode(block, 'Port', Blockly.Python.ORDER_ATOMIC);
        var value_assign_to = Blockly.Python.valueToCode(block, 'Assign To', Blockly.Python.ORDER_ATOMIC);
        // TODO: Assemble Python into code variable.
        var code = 'obj.loop_data({"Database Type":'+dropdown_database_type+', "Server":'+value_server+', "Username":'+value_username+', "Password":'+value_password+', "Database Instance Name":'+value_database_instance_name+', "SQL Query":'+value_sql_query+', "Port":'+value_port+', "Assigned To":'+value_assign_to+'})\n';
        return code;
      };


    Blockly.Python['execute_script'] = function(block) {
        var generate_log = block.getFieldValue('generateLog');
        // var logFileType = block.getFieldValue('logFileType');
        var script_name = Blockly.Python.valueToCode(block, 'Script name', Blockly.Python.ORDER_ATOMIC);
        var value_path_to_script = Blockly.Python.valueToCode(block, 'Path to script', Blockly.Python.ORDER_ATOMIC);
        var log_file_name = Blockly.Python.valueToCode(block, 'Log File name', Blockly.Python.ORDER_ATOMIC);
        var value_path_to_log = Blockly.Python.valueToCode(block, 'Path to Log File', Blockly.Python.ORDER_ATOMIC);
        // TODO: Assemble Python into code variable.
        console.log(value_path_to_script)
        // var code = 'import subprocess\nsubprocess.call(["python3", '+ value_path_to_script +'])\n';
        var code = 'obj.execute_script({"Script name":"'+script_name+'", "Path to Script":"'+ value_path_to_script +'", "Generate Log" : '+generate_log+', "Log File name":"'+log_file_name+'", "Path to Log File":"'+ value_path_to_log +'"})\n'
        return code;
    };


    Blockly.Python['var_socialDistancing'] = function(block) {
        // TODO: Assemble Python into code variable.
        var code = "obj.social_distancing()";
        // TODO: Change ORDER_NONE to the correct strength.
        return [code, Blockly.Python.ORDER_NONE];
    };

    Blockly.Python['var_temperature'] = function(block) {
        // TODO: Assemble Python into code variable.
        var code = "obj.temperature()";
        // TODO: Change ORDER_NONE to the correct strength.
        return [code, Blockly.Python.ORDER_NONE];
    };

    Blockly.Python['var_faceCovering'] = function(block) {
        // TODO: Assemble Python into code variable.
        var code = "obj.face_covering()";
        // TODO: Change ORDER_NONE to the correct strength.
        return [code, Blockly.Python.ORDER_NONE];
    };

    Blockly.Python['var_facialRecognition'] = function(block) {
        // TODO: Assemble Python into code variable.
        var code = "obj.facial_recognition()";
        // TODO: Change ORDER_NONE to the correct strength.
        return [code, Blockly.Python.ORDER_NONE];
    };
      
    Blockly.Python['input_string'] = function(block) {
        var text_string = block.getFieldValue('string');
        // TODO: Assemble Python into code variable.
        var code = text_string;
         // TODO: Change ORDER_NONE to the correct strength.
        return [code, Blockly.Python.ORDER_ATOMIC];
    };
      
    Blockly.Python['send_text'] = function(block) {
        var text_text = block.getFieldValue('text');
        // TODO: Assemble JavaScript into code variable.
        var code = '...;\n';
        return code;
    };

    Blockly.Python['trigger_led'] = function(block) {
        var dropdown_led = block.getFieldValue('led');
        var dropdown_on_off = block.getFieldValue('on_off');
        var dropdown_colors = block.getFieldValue('colors');
        // TODO: Assemble JavaScript into code variable.
        var code = '...;\n';
        return code;
    };

    Blockly.Python['webservice_call'] = function(block) {
        // TODO: Assemble Python into code variable.
        var code = '...\n';
        return code;
    };

    Blockly.Python['send_email'] = function(block) {
        var to_list = Blockly.Python.valueToCode(block, 'To Email', Blockly.Python.ORDER_ATOMIC);
        var subject = Blockly.Python.valueToCode(block, 'Script name', Blockly.Python.ORDER_ATOMIC);
        var body = Blockly.Python.valueToCode(block, 'Script name', Blockly.Python.ORDER_ATOMIC);
        // TODO: Assemble Python into code variable.
        var code = '...\n';
        return code;
    };
}

