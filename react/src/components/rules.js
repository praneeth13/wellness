import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import Blockly from 'node-blockly/browser'
import BlocklyDrawer, { Block, Category } from 'react-blockly-drawer'
import {addCustomBlocks} from './customblocks' 
import axios from 'axios';
import { baseUrl,baseUrlApi, headers } from "../config";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ToastContainer, toast } from 'react-toastify';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button' ;
import Form from 'react-bootstrap/Form' ;
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css'; 
import { faDownload, faPlus, faSave, faUpload  } from "@fortawesome/free-solid-svg-icons";
import { faPlayCircle , faSortDown , faSortUp,  faTrash, faEdit} from "@fortawesome/free-solid-svg-icons";

const config = {
    headers: {
        'content-type': 'multipart/form-data',
        'Access-Control-Allow-Origin' : '*',
    }    
}

class Rules extends PureComponent {
    constructor(props) {
        super(props)

        this.state = {
            galleryIsOpen : false,
            outputImages : ['https://www.pyimagesearch.com/wp-content/uploads/2020/04/face_mask_detection_result01.jpg'], 
            photoIndex : 0,
            showConditions : true,
            showOperands : false,
            showLoops : false,
            showProcesses : false,
            showVariables : false,
            workspace : '',
            code : '',
            consoleLog : '',
            queryList : [],
        }
    }

    onRun = () => {
        const formData = new FormData();
        formData.append('code',this.state.code)
        if(this.state.selectedFiles)
        this.state.selectedFiles.map(file => formData.append('input_image',file)) 

        axios.post(baseUrlApi + `/getPythonFile`,formData,config)
            .then(res => {
            console.log(res) ;
            if (res.data.status === 200) {
                toast.success(res.data.data, {
                    position: "top-right"
                });
                // this.setState({ 
                //     isOpen: true,
                //     outputImages: imagePaths 
                // });

                // this.updateConsole = setInterval(() => {
                //     axios.get(baseUrl + `/getLog`).then(res => { 
                //         let consoleLog = res.data.data
                //         this.setState({ 
                //             consoleLog: consoleLog,
                //         });
                //     });
                // }, 1000);
            }
            else {
                toast.error(res.data.data, {
                    position: "top-right"
                }); 
            }

        })    
    }

    handleLoad = () => {
        axios.get(baseUrlApi + `/api/conditions/get`,config).then(res => { 
            this.setState({
                queryList : res.data.data,
                loadModalShow: true 
            })
        })
    }
    
    handleEditQuery = (query) => {
        this.setState({
            workspace : query,
            loadModalShow: false
        })
    }
    
    
    handleDelete = (id) => {
        axios.delete(baseUrlApi + "/api/conditions/delete/"+id["$oid"]).then(res => { 
            console.log(res.data)
            this.handleLoad()
            // this.setState({
            //     queryList : res.data
            // })
            // this.setState({ loadModalShow: true })
        })
    }
    
    handleSave = () => {
        let xml = this.state.workspace ;
        let name = document.getElementById("StateName").value;
        console.log(xml)
        const formData = new FormData();
        formData.append('xml',xml)
        formData.append('name',name)
    
        axios.post(baseUrlApi + `/api/conditions/insert`,formData,config).then(res => { 
          console.log(res.data) 
          if (res.data.status === 200) {
              toast.success( "Expression saved successfully", {
                  position: "bottom-right"
              });
              console.log("Success")
          }
          else{
              toast.error("Error", {
                  position: "bottom-right"
              }); 
              console.log("Failed")
          }
          this.setState({saveModalShow: false})
        })
    }

    componentWillMount(){
        addCustomBlocks(Blockly)
    }

    componentDidMount(){
        Blockly.getMainWorkspace().setTheme(Blockly.Themes.Dark);  
        this.handleCollapsableButtons()
    }
    
    componentDidUpdate(){
        this.handleCollapsableButtons()
        // console.log(Blockly.getMainWorkspace())
    }

    handleCollapsableButtons = () => {
        let buttons = document.getElementsByClassName("blocklyFlyoutLabel")
        let labels = ["showConditions", "showOperands", "showLoops", "showProcesses", "showVariables"]
        for(let i = 0 ; i < 5 ; i++){
            buttons[i].onclick = () => {
                this.setState({[labels[i]] : this.state[labels[i]] ? false : true })
            }
        }
    }

    render() {
        let loadModalClose = () => this.setState({loadModalShow: false});
        let saveModalClose = () => this.setState({saveModalShow: false});
        let {galleryIsOpen, outputImages, photoIndex} = {...this.state}
        return (
            <div className="blockly">
                <section role="main" className="content-body">
                    <button style={{position:"fixed",top:"140px",right:"40px", zIndex:"5"}} className="btn btn-success btn-sm" onClick={this.onRun} for="myInput">Execute <FontAwesomeIcon icon={faPlayCircle} /></button>
                    <button style={{position:"fixed",top:"140px",right:"150px", zIndex:"5"}} className="btn btn-secondary btn-sm" onClick={this.handleLoad} for="myInput">Load <FontAwesomeIcon icon={faDownload} /></button>
                    <button style={{position:"fixed",top:"140px",right:"240px", zIndex:"5"}} className="btn btn-danger btn-sm" onClick={() => this.setState({saveModalShow: true})} for="myInput">Save <FontAwesomeIcon icon={faSave} /></button>
                    <button style={{position:"fixed",top:"140px",right:"330px", zIndex:"5"}} className="btn btn-warning btn-sm" onClick={() => this.setState({workspace : ''})} for="myInput">New <FontAwesomeIcon icon={faPlus} /></button>
                    <button style={{position:"fixed",top:"140px",right:"415px", zIndex:"5"}} className="btn btn-info btn-sm" onClick={() => this.setState({galleryIsOpen:true})} for="myInput">Gallery <FontAwesomeIcon icon={faPlus} /></button>
                    <header className={'page-header ' + (this.state.showMenu ? '' : 'header-left')}>
                        <h2>Policy Builder</h2>
                    </header>
                    <BlocklyDrawer
                        onChange={(code, workspace) => {
                            this.state.workspace = workspace
                            this.state.code = code
                            console.log(workspace)
                            console.log(code)

                        }}
                        injectOptions = {{grid:{spacing: 30,length: 3,colour: '#474444',snap: true},}}
                        workspaceXML = {this.state.workspace}
                        language = {Blockly.Python}
                    >
                        <label web-class="btn" text="Conditions"></label>
                        {this.state.showConditions ?
                            <>
                      
                            <Block type="controls_if"></Block> 
                            <Block type="controls_if"> 
                                <mutation else="1"></mutation> 
                            </Block> 
                            <Block colour="120" type="controls_if"> 
                                <mutation elseif="1" else="1"></mutation> 
                            </Block> 
                            <Block colour="120" type="logic_ternary"></Block> 
                            </>
                        : null    
                        }


                        <label web-class="btn" text="Operands"></label>
                        {this.state.showOperands ?
                            <>
                            <Block type="logic_compare"> 
                                <field name="OP">EQ</field> 
                            </Block> 
                            <Block type="logic_operation"> 
                                <field name="OP">AND</field> 
                            </Block> 
                            <Block type="logic_negate"></Block> 
                            <Block type="input_string"></Block>
                            <Block type="math_number">
                                <field name="NUM">0</field>
                            </Block>
                            <Block type="logic_boolean">
                                <field name="BOOL">TRUE</field>
                            </Block>
                            </>
                        : null    
                        }


                        <label web-class="btn" text="Loops"></label>
                        {this.state.showLoops ?
                            <>
                            <Block type="controls_repeat_ext"> 
                                <value name="TIMES"> 
                                <shadow type="math_number"> 
                                    <field name="NUM">10</field> 
                                </shadow> 
                                </value> 
                            </Block> 
                            <Block type="controls_whileUntil"> 
                                <field name="MODE">WHILE</field> 
                            </Block> 
                            </>
                        : null    
                        }


                        <label web-class="btn" text="Processes"></label>
                        {this.state.showProcesses ?
                            <>
                            <Block type="sendemail"></Block>
                            <Block type="send_text"></Block>
                            <Block type="trigger_led"></Block>
                            <Block type="webservice_call"></Block>
                            <Block type="database_query"></Block> 
                            <Block type="database_insert"></Block> 
                            <Block type="write_to_file"></Block> 
                            <Block type="execute_script"></Block> 
                            </>
                        : null    
                        }


                        <label web-class="btn" text="Variables"></label> 
                        {this.state.showVariables ?
                            <>
                            <Block type="var_socialDistancing"></Block> 
                            <Block type="var_temperature"></Block> 
                            <Block type="var_faceCovering"></Block> 
                            <Block type="var_facialRecognition"></Block> 
                            </>
                        : null    
                        }


                        {/* <Category colour="120" name="Conditions" expanded="true">
                            <Block type="controls_if"></Block> 
                            <Block type="controls_if"> 
                                <mutation else="1"></mutation> 
                            </Block> 
                            <Block colour="120" type="controls_if"> 
                                <mutation elseif="1" else="1"></mutation> 
                            </Block> 
                            <Block colour="120" type="logic_ternary"></Block> 
                        </Category>

                        <Category colour="120" name="Operands">
                            <Block type="logic_compare"> 
                                <field name="OP">EQ</field> 
                            </Block> 
                            <Block type="logic_operation"> 
                                <field name="OP">AND</field> 
                            </Block> 
                            <Block type="logic_negate"></Block> 
                        </Category>

                        <Category colour="120" name="Loops">
                            <Block type="controls_repeat_ext"> 
                                <value name="TIMES"> 
                                <shadow type="math_number"> 
                                    <field name="NUM">10</field> 
                                </shadow> 
                                </value> 
                            </Block> 
                            <Block type="controls_whileUntil"> 
                                <field name="MODE">WHILE</field> 
                            </Block> 
                        </Category>

                        <Category colour="120" name="Processes">
                            <Block type="database_query"></Block> 
                            <Block type="database_insert"></Block> 
                            <Block type="write_to_file"></Block> 
                            <Block type="execute_script"></Block> 
                        </Category>

                        <Category colour="120" name="Variables">
                            <Block type="var_socialDistancing"></Block> 
                            <Block type="var_temperature"></Block> 
                            <Block type="var_faceCovering"></Block> 
                            <Block type="var_facialRecognition"></Block> 
                        </Category> */}
                    </BlocklyDrawer>    
                </section>
                <Modal
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    show= {this.state.loadModalShow}
                    onHide={loadModalClose}
                    >
                    <div className="bg-dark" >
                        <Modal.Header>
                            <Modal.Title class="modalText" id="contained-modal-title-vcenter">
                                <h2>Load</h2>
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                        <div className="container modalText">
                                <div>
                                    {this.state.queryList.map((query)=>
                                        
                                    <div style={{fontSize:"26px", color:"white"}}>
                                        {query.name} 
                                        <Button size="sm" variant="danger" onClick={() => this.handleDelete(query._id)} style={{float: "right",margin:"2px"}}><FontAwesomeIcon icon={faTrash} title="Delete"/></Button>                                                      
                                        <Button size="sm" variant="success" onClick={() => this.handleEditQuery(query.xml_data)} style={{float: "right",margin:"2px"}}>Edit <FontAwesomeIcon icon={faEdit} title="Edit" /></Button>
                                    </div>                                                        
                                    )} 
                                </div>
                        </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={loadModalClose}>Close</Button> 
                        </Modal.Footer>
                    </div>
                </Modal>
                <Modal
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    show= {this.state.saveModalShow}
                    onHide={saveModalClose}
                >
                    <div className="bg-dark" >
                        <Modal.Header >
                            <Modal.Title class="modalText" id="contained-modal-title-vcenter">
                                <h3>Save</h3>
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                        <div className="container modalText">
                            <Form.Control id="StateName" type="text" placeholder="Name" />
                        </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={saveModalClose}>Close</Button> 
                            <Button className="btn btn-success" onClick={this.handleSave}>Save</Button>
                        </Modal.Footer>
                    </div>
                </Modal>
                {galleryIsOpen && (
                    <Lightbox
                        mainSrc={outputImages[photoIndex]}
                        nextSrc={outputImages[(photoIndex + 1) % outputImages.length]}
                        prevSrc={outputImages[(photoIndex + outputImages.length - 1) % outputImages.length]}
                        onCloseRequest={() => this.setState({ galleryIsOpen: false })}
                        onMovePrevRequest={() =>
                        this.setState({
                            photoIndex: (photoIndex + outputImages.length - 1) % outputImages.length,
                        })
                        }
                        onMoveNextRequest={() =>
                        this.setState({
                            photoIndex: (photoIndex + 1) % outputImages.length,
                        })
                        }
                    /> 
                )}
                <ToastContainer />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = (dispatch) => ({
    
})

export default connect(mapStateToProps, mapDispatchToProps)(Rules)
