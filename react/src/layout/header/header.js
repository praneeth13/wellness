import React, { Component } from 'react';
import { withRouter} from 'react-router-dom';
import logoImage from '../../assets/images/logo-web.png';
import userImage from '../../assets/images/user.jpg';
import { NavLink } from 'react-router-dom';
import { connect } from "react-redux";
import * as userAction from "../../actions/user.action";
import _ from 'lodash'
import { ToastContainer, toast } from 'react-toastify';
import { baseUrl, baseUrlApi } from '../../config';
import { stat } from 'fs';
 
class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayMenu:false,
      profile:this.props.profile,
      activeType:this.props.activeType
    }
  }
  
  componentDidMount(){
    if(window.location.pathname==="/scan"){
      this.setState({showPatientDetailsButton:true})
    }
    this.props.getMyProfile()
  // this.setState({profile:this.props.profile})
   }
  componentWillReceiveProps(nextProps){
 if(!_.isEmpty(nextProps.errorObj)){
  console.log(nextProps.errorObj)
  // toast.error(nextProps.errorObj.message.message, {
  //   position: toast.POSITION.TOP_RIGHT
  //   });
   if(nextProps.errorObj.status===400){
    // this.logOut()
   }
      
}   
     this.setState({profile:nextProps.profile})
  }
  logOut = () => {
    // this.props.newSession()
    localStorage.removeItem('wellness-token')
    this.props.history.push('/')
}
HistoryPush=(path)=>{
  this.props.history.push(path)
  console.log("check")
  this.setState({ displayMenu: false})

}
goToChangePassword=()=>{
  this.props.history.push('/change-password')
}
lockScreen=()=>{
  this.HistoryPush('/lock-screen')
}

  render() {
     return (
      <div>
        <header className="header header-nav-menu header-nav-stripe">
                <div className="logo-container">
					<a className="logo">
          <img src={logoImage} className="c-pointer" width="100" height="50" alt="logo" onClick={()=>{this.HistoryPush('/home')}}/>
          </a>
				 
					<div className="header-nav collapse">
						<div className="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
							<nav>
								{/* <ul className="nav nav-pills" id="mainNav">
									<li className="colorblue">
                      <NavLink to="/patient_management"  exact className={this.state.activeType==='session'?"nav-link menu-active mr-2":"nav-link mr-2 fs-18"} >
									        <i className="fas fa-american-sign-language-interpreting"></i> header navigation 1
                      </NavLink> 
                      
                      
									</li>
                  <li className="colorblue">
                  <NavLink to="/training-model"  exact className={this.state.activeType===''?"nav-link menu-active":"nav-link "} >
									        <i className="fas fa-american-sign-language-interpreting"></i> header navigation 2
                      </NavLink> 
                      
                  </li>
                </ul> */}
                </nav>
              </div>
           </div>
				</div>
				<span className="separator"></span>
				<div className="header-right">
					<span className="separator"></span>
					<div id="userbox" className="userbox">
                <div  className="pointer" data-toggle="dropdown"
                    onClick={e=>this.setState({ displayMenu: !this.state.displayMenu })} 
                    onBlur = {e=>{
                      setTimeout(() => {
                        this.setState({ displayMenu: false })
                      }, 10);
                    }}
                    tabIndex="0"
                >
                    <figure className="profile-picture"> <img src={this.state.profile.profile_img==='None'?userImage:baseUrlApi+"/"+this.state.profile.profile_img} alt="Joseph Doe" className="rounded-circle"/></figure>
                    <div className="profile-info">
                        <span className="name">{!_.isEmpty(this.state.profile)?(this.state.profile.first_name||"John")+" "+(this.state.profile.last_name||"Doa"):"Praneeth"}</span>
                        <span className="role mt-1">{!_.isEmpty(this.state.profile)&&this.state.profile.userType===2?"User":"Admin"}</span>
                    </div>
                    {this.state.displayMenu ? <i class="fas fa-chevron-up"></i>
                    :
                    <i className="fas fa-chevron-down" ></i>
                    }
                    {this.state.displayMenu ? (
                      <ul className="list-unstyled mb-2 admin-dropdown position-absolute p-2">
                          <li><a tabIndex="" className="menuitem" onClick={()=>{this.HistoryPush('/profile')}}><i className="fas fa-user pr-2"></i> My Profile</a> </li>
                          <li><a tabIndex="" className="menuitem" onClick={()=>{this.HistoryPush('/change-password')}}><i className="fas fa-user pr-2"></i> Change Password</a> </li>
                          <li><a tabIndex="" className="menuitem" onClick={e=>this.logOut(e)}><i className="fas fa-power-off pr-2"></i> Logout</a></li>
                      </ul>
                  ) :
                  null} 
                </div>
                
               
            </div>
				</div>
			</header>
       <ToastContainer />
      </div>

    )
  }
}
 
const mapStateToProps = (state) => {
  return {
    profile:state.login.my_profile,
    errorObj:state.login.errorObj
   }
}
const mapDispatchToProps = (dispatch) => {
return {
  getMyProfile:(obj)=>{return dispatch(userAction.getMyProfile())}

}
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));

