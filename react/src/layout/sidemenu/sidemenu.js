
import React, { Component } from 'react';
import 'react-toastify/dist/ReactToastify.css';
import { withRouter } from "react-router";
import { connect } from "react-redux";
import _ from 'lodash'
import Loader from 'react-loader-spinner'
import { ToastContainer, toast } from 'react-toastify';
import * as userAction from "../../actions/user.action";

class Sidemenu extends Component {

    constructor() {
        super();
        this.state = {
            showLoader: false,
            showMenu: false,
        }
    }
    componentDidMount() {

        if (localStorage.getItem("wellness-token")) {
            // this.props.getMyProfile()

        } else {
            this.HistoryPush('/')
        }
    }

    handleChange = (event) => {
        if (event.target.name === "translator") {
            event.target.value = parseInt(event.target.value)

        }
        this.setState({ [event.target.name]: event.target.value })
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.tokenExprired === true) {
            toast.warn("token expired", {
                position: toast.POSITION.TOP_RIGHT
            });
            localStorage.removeItem('wellness-token')
            setTimeout(() => {
                this.HistoryPush("/")

            }, 2000);
        }
    }
    HistoryPush = (path) => {
        this.props.history.push(path)
    }

    openSidemenu = () => {
        setTimeout(() => {
            this.setState({showMenu : true})
        }, 20);
    }

    closeSidemenu = () => {
        setTimeout(() => {
            this.setState({showMenu : false})
        }, 20);
    }

    render() {
         return (
            <div className="">
                {this.state.showLoader ? (
                    <div className="loading" >
                        <div className="loader">
                            <Loader
                                type="Watch"
                                color="#00BFFF"
                                height="100"
                                width="100"
                            />
                        </div>
                    </div>
                ) : null}

                <aside className={'sidebar-left ' + (this.state.showMenu ? '' : ' own-sidemenu-main closed')} id="sidebar-left">

                    <div className="sidebar-header">
                        <div className="sidebar-toggle d-none d-md-block">
                            <i className="fas fa-bars" aria-label="Toggle sidebar" onClick={this.openSidemenu}></i>
                        </div>
                    </div>

                    <div className="nano has-scrollbar" onMouseLeave={this.closeSidemenu}>
                        <div className="nano-content" tabIndex="0">
                            <nav id="menu" className="nav-main" role="navigation">
                                <ul className="nav nav-main"  >
                                <li className="c-pointer" onMouseEnter={this.openSidemenu}>
                                        <a className="nav-link fs-18" onClick={() => {this.closeSidemenu() ; this.HistoryPush('/rules') }}>
                                            <i class="fas fa-capsules"></i>
                                            <span>Policy Builder</span>
                                        </a>
                                    </li>
                                    <li className="c-pointer" onMouseEnter={this.openSidemenu} >
                                        <a className="nav-link fs-18" onClick={() => {this.closeSidemenu() ; this.HistoryPush('/dashboard') }}>
                                             <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                            <span>Reports Alert</span>
                                        </a>
                                    </li>
                                    <li className="c-pointer" onMouseEnter={this.openSidemenu}>
                                        <a className="nav-link fs-18" onClick={() => {this.closeSidemenu() ; this.HistoryPush('/map') }}>
                                            <i class="fas fa-globe-americas"></i>
                                            <span>Map</span>
                                        </a>
                                    </li>
                                    <li className="c-pointer" onMouseEnter={this.openSidemenu}>
                                        <a className="nav-link fs-18" onClick={() => {this.closeSidemenu() ; this.HistoryPush('/events') }}>
                                             <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                            <span>Events</span>
                                        </a>
                                    </li>
                                    <li className="c-pointer" onMouseEnter={this.openSidemenu}>
                                        <a className="nav-link fs-18" onClick={() => {this.closeSidemenu() ; this.HistoryPush('/matrics') }}>
                                            <i class="fas fa-chart-bar"></i>
                                            <span>Metrics</span>
                                        </a>
                                    </li>
                                    <li className="c-pointer" onMouseEnter={this.openSidemenu}>
                                        <a className="nav-link fs-18" onClick={() => {this.closeSidemenu() ; this.HistoryPush('/viewfeed') }}>
                                            <i class="fa fa-desktop" aria-hidden="true"></i>
                                            <span>Monitor</span>
                                        </a>
                                    </li>
                                    <li className="c-pointer" onMouseEnter={this.openSidemenu}>
                                        <a className="nav-link fs-18" onClick={() => {this.closeSidemenu() ; this.HistoryPush('/admin') }}>
                                            <i className="fas fa-cog"></i>
                                            <span>Admin</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>

                </aside >
                <ToastContainer />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        tokenExprired: state.login.tokenExprired,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        getMyProfile: () => { dispatch(userAction.getMyProfile()) }


    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Sidemenu));

