import * as types from "../actions/types";
import store from '../store'
import { baseUrl } from "../config";
import Axios from 'axios'



export const login = (loginObj) => {
    return Axios.post(baseUrl+`/users/login`,loginObj).then(res=>{
      let  response = res.data
             return  response
         }) 
}

export const sendOPT = (mail) => {
        return Axios.post(baseUrl+`/users/send_reset_password`,{email:mail}).then(res=>{
          let  response = res['data']
                 return  response
             }).catch(err => {
                 return err
        })
}

export const reset_password = (obj) => {
     return Axios.post(baseUrl+`/users/reset_password`,obj).then(res=>{
          let  response = res['data']
                 return  response
             }).catch(err => {
                 return err
        })
}

export const registerUser = (obj) => {
  
 
        return Axios.post(baseUrl+`/users/create`,obj).then(res=>{
            console.log(res)

          let  response = res['data']
                 return  response
             }).catch(err => {
                 console.log(err)
                 return err
        })
}
export const changePassword = (userId,obj) => {
         return Axios.post(baseUrl+`/users/change_password?user_id=${userId}`,obj).then(res=>{
          let  response = res['data']
                  return  response
             }).catch(err => {
                 return err
        })
}

export const getAllUsers = (limit,pageNumber) => {
    return Axios.get(baseUrl+`/users/getUsers`, {
        params: {
          limit: limit,
          pagenumber: pageNumber
        }
      }).then(res=>{
     let  response = res['data']
             return  response
        }).catch(err => {
            return err
   })
}

export const deleteUser = (id) => {
    return Axios.delete(baseUrl + `/users/delete?user_id=${id}`).then(res => {
        let response = res['data']
        return response
    }).catch(err => {
        return err
    })
}


 
