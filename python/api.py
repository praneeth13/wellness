#!/usr/bin/env bash
import os
from flask import Flask, render_template, request, send_file, send_from_directory, Response
from flask import jsonify
import cv2
import numpy as np
from flask_cors import CORS
from flask_cors import cross_origin
import mysql.connector as sql
import subprocess
import threading
import io
from PIL import Image
from io import BytesIO
from io import StringIO  # for Python 3
import user
import face_recognition
import re
import base64
# from config import URL
import flirimageextractor
from matplotlib import cm

# app = Flask(__name__)


KNOWN_FACES_PATH = 'profile_pictures'

app = Flask(__name__, static_folder='static')
app.register_blueprint(user.app, url_prefix='/wellness/users')

DEFAULT_VIEW_MAP_LATITUDE = 39.74
DEFAULT_VIEW_MAP_LONGTITUDE = -104.99

faces = []
names = []
host = 'localhost'
user = 'root'
password = 'Praneeth123'
db = 'wellness'
con = sql.connect(host=host, user=user, password=password,
                  db=db, use_unicode=True, charset='utf8')

flir = flirimageextractor.FlirImageExtractor(palettes=[cm.jet, cm.bwr, cm.gist_ncar])

folderdir = "video/24"
currentDirectory = os.getcwd()
cors = CORS(app, resources={r"/*": {"origins": "*"}})
liveObj = {}
CAMERA = ''
# @app.route('/', methods=['GET'])
# def test():
#     return "Hello World !!"


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/getPythonFile', methods = ['POST'])
@cross_origin()
def get_python_file():
	try:
		python_script = request.form['code']
		python_file = open("code.py", 'w')
		python_file.write("from constants import Constants\nobj = Constants()\n")
		python_file.write(python_script)
		python_file.close()
		# frames = request.files.getlist('input_image')
		# if not os.path.exists("./static"):
		# 		os.mkdir("./static")
		# if not os.path.exists("./static/original"):
		# 		os.mkdir("./static/original")
		# for frame in frames:
		# 	frame = frame.read()
		# 	numpyimg = np.frombuffer(frame, np.uint8)
		# 	orgImg = cv2.imdecode(numpyimg, cv2.IMREAD_COLOR)
		# 	cv2.imwrite("./static/original/image"+str(randint(0,100))+".jpg",orgImg)
		subprocess.call(["python3", "code.py"])
		status={"status":200,"data":"Expression Executed Successfully"}
	except Exception as e:
		status = {"status":400, "data":"Error:"+str(e)}
	return jsonify(status)

# Get All Conditions
@app.route('/api/conditions/get', methods=['GET'])
@cross_origin()
def getAllConditions():
	try:
		sql_query = "SELECT condition_name, condition_path FROM conditions WHERE user_id = %d" % (int(1))
		cursor.execute(sql_query)
		results = cursor.fetchall()
		xmllist = []
		for result in results:
			data = {}
			data['name'] = result[0]
			with open(result[1], "r") as xml_file:
				data['xml_data'] = xml_file.read()
				xml_file.close()
			xmllist.append(data)
		status = {"status":200, "data": xmllist}
	except Exception as e:
		status = {"status":400, "data":"Error:"+str(e)}
	return json.dumps(status, default=json_util.default)

# Get a single condition
@app.route('/api/conditions/find/<name>', methods=['GET'])
@cross_origin()
def getCondition(name):
	try:
		sql_query = "SELECT condition_name, condition_path FROM conditions WHERE condition_name = '%s'" % (str(name))
		cursor.execute(sql_query)
		result = cursor.fetchone()
		data = {}
		data['name'] = result[0]
		with open(result[1], "r") as xml_file:
				data['xml_data'] = xml_file.read()
				xml_file.close()
		status = {"status":200, "data":data}
	except Exception as e:
		status = {"status":400, "data":"Error:"+str(e)}	
	return json.dumps(status, default=json_util.default)

# Delete a condition
@app.route('/api/conditions/delete/<name>', methods=['DELETE'])
@cross_origin()
def deleteCondition(name):
	try:
		sql_query = "DELETE FROM conditions WHERE condition_name = '%s'" % (str(name))
		cursor.execute(sql_query)
		con.commit()
		status = {"status":"200", "data":"Deleted"}
	except Exception as e:
		status = {"status":400, "data":"Error:"+str(e)}
	return jsonify(status)

@app.route('/addProfilePicture', methods=['post'])
@cross_origin()
def add_profile_picture():
    try:
        id = request.form['id']
        base64img=request.form['profile_img']
        username=request.form['username']
        data = re.sub('^data:image/.+;base64,', '', base64img)
        data = base64.b64decode(data)
        frame = np.fromstring(data, np.uint8)
        frame = cv2.imdecode(frame, cv2.IMREAD_COLOR)
        # Writing to file
        cv2.imwrite(KNOWN_FACES_PATH+"/"+username+".jpg", frame)
        loadFaces()
        # Updating in Database.
        sql_query = "UPDATE users SET profile_img = ('%s') WHERE id = '%d'" % (
            KNOWN_FACES_PATH+"/"+username+".jpg",
            int(id)
        )
        cursor = con.cursor()
        cursor.execute(sql_query)
        con.commit()
        
        status = {"status": 200, "result": "uploaded"}
    except Exception as e:
        status = {"status": 500, "result": str(e)}
    return responseHandler(202,status)

@app.route('/getframes', methods=['GET'])
@cross_origin()
def getFrames():
    try:
        frames = []
        for frame in os.listdir(currentDirectory+"/"+folderdir):
            obj = {}
            obj['file_path'] = frame
            frames.append(obj)
        return jsonify({"status": 200, "data": frames})
    except:
        return jsonify({"status": 500, "message": "Something Went Wrong!!"})


# @app.route('/static/<imagename>',methods=['get'])
# @cross_origin()
# def sendImage(imagename):
#     fileName = currentDirectory+"/"+folderdir+"/"+imagename
#     return send_file(fileName)
def loadFaces():
    print("Loading Faces......")
    for image_name in os.listdir(KNOWN_FACES_PATH):
        known_image = face_recognition.load_image_file(KNOWN_FACES_PATH+'/'+image_name)
        biden_encoding = ""
        if face_recognition.face_encodings(known_image):
            biden_encoding =  face_recognition.face_encodings(known_image)[0]
        faces.append(biden_encoding)
        names.append(image_name)
    print("Loading Completed.....")

@app.route('/getpoints', methods=['GET'])
@cross_origin()
def getPoints():
    try:
        points = []
        cursor = con.cursor()
        sql = "SELECT * FROM map_cameras WHERE is_active = %s"
        cursor.execute(sql, (1,))
        data = cursor.fetchall()
        print(data)
        for row in data:
            obj = {}
            obj['id'] = row[0]
            obj['name'] = row[1]
            obj['type'] = row[2]
            obj['address'] = row[3]
            obj['lat'] = row[4]
            obj['long'] = row[5]
            obj['is_active'] = row[6]
            obj['notes'] = row[7]
            points.append(obj)
        return jsonify({"status": 200, "data": points, "lat": DEFAULT_VIEW_MAP_LATITUDE, "long": DEFAULT_VIEW_MAP_LONGTITUDE})
    except Exception as e:
        print(e)
        return jsonify({"status": 500, "message": "Something Went Wrong!!"})



@app.route('/getvideosbycamera', methods=['POST'])
@cross_origin()
def getVideosByCamera():
    try:
        print("API Called")
        reqData = request.get_json()
        queryDate = reqData['date'].split("T")[0]
        videos = []
        cursor = con.cursor()
        sql = "SELECT * FROM map_camera_videos WHERE camera_id = %s and video_date = %s"
        cursor.execute(sql, (reqData['camera_id'], queryDate,))
        print(reqData['camera_id'], queryDate)
        data = cursor.fetchall()
        con.commit()
        print("Query Called")
        print(videos)
        for row in data:
            print(row)
            obj = {}
            obj['id'] = row[0]
            obj['camera_id'] = row[1]
            obj['video_path'] = row[2]
            obj['thumbail_path'] = row[3]
            obj['video_duration'] = row[4]
            obj['video_name'] = row[5]
            obj['video_date'] = row[6]
            obj['start_time'] = row[7]
            obj['end_time'] = row[8]
            videos.append(obj)
        return jsonify({"status": 200, "data": videos})
    except Exception as e:
        print(e)
        return jsonify({"status": 500, "message": "Something Went Wrong!!"})


# @app.route('/videos/<cameraid>/<filename>', methods=['get'])
# @cross_origin()
# def viewRecordedVideo(cameraid, filename):
#     # fileName = currentDirectory+"/1/2020-02-06 15:04:25.900493.mp4"
#     fileName = "videos/"+cameraid+"/"+filename
#     print(fileName)
#     return send_file(fileName, as_attachment=False)

@app.route('/videos', methods=['get'])
@cross_origin()
def get_videos():
    try:
        date = request.form['date'] # In format YYYY-MM-DD
        sql_query = "SELECT video_path FROM map_camera_videos WHERE video_date = '%s'" %(date)
        # sql_query = "SELECT video_path FROM map_camera_videos"
        cursor = con.cursor()
        cursor.execute(sql_query)
        results = cursor.fetchall()
        video_paths = []
        for result in results:
            video_paths.append("static/videos/1"+str(result[0]))
        status = {
            'data': video_paths,
            'status': 200
        }
    except Exception as e:
        status = {
            "data": str(e),
            'status': 400
        }
    return jsonify(status)

def recordVideos():
    cursor = con.cursor()
    sql = "SELECT * FROM map_cameras WHERE is_active = %s "
    cursor.execute(sql, (1,))
    data = cursor.fetchall()
    count = 0
    for row in data:
        count += 1
        if count < 3:
            if count == 1:
                cap = cv2.VideoCapture(0)
            else:
                cap = cv2.VideoCapture(2)
            command = ['python', './record.py', '--cameraID', str(row[0])]
            process = subprocess.Popen(command)
            updateSubPID(row[0], process.pid)


def gen(cameraID):
    image_path = "./static/videos/"+str(cameraID)+"/liveframe.jpg"
    while True:
        with open(image_path, 'rb') as f:
            check_chars = f.read()[-2:]
        if check_chars != b'\xff\xd9':
            pass
            # print('Not complete image')
        else:
            # imrgb = cv2.imread(os.path.join(path, file), 1)
            frame = cv2.imread(image_path)

            if frame is not None:
                ret, jpeg = cv2.imencode('.jpg', frame)
                yield (b'--frame\r\n'
                    b'Content-Type: image/jpeg\r\n\r\n' + jpeg.tobytes() + b'\r\n\r\n')
            else:
                pass

def facial_recognition(cameraID):
    image_path = "./static/videos/"+str(cameraID)+"/liveframe.jpg"
    while True:
        with open(image_path, 'rb') as f:
            check_chars = f.read()[-2:]
        if check_chars != b'\xff\xd9':
            pass
            # print('Not complete image')
        else:
            # imrgb = cv2.imread(os.path.join(path, file), 1)
            frame = cv2.imread(image_path)
            if frame is not None:
                face_locations = face_recognition.face_locations(frame)
                face_encodings = face_recognition.face_encodings(frame, face_locations)
                recognized_faces = []
                try:
                    for face_encoding in face_encodings:
                        matches = face_recognition.compare_faces(faces, face_encoding)
                        name = 'Unknown'
                        face_distances = face_recognition.face_distance(faces, face_encoding)
                        best_match_index = np.argmin(face_distances)
                        if matches[best_match_index]:
                            name = names[best_match_index][:-4]
                        #     cursor = con.cursor()
                        #     sql_query = "SELECT id,username,password,profile_img,email,phone,userType,first_name,last_name,city,state,address,zipcode,gender FROM users WHERE username = '%s'" % (str(name))
                        #     cursor.execute(sql_query)
                        #     result = cursor.fetchone()
                        #     person = {
                        #         'id': result[0],
                        #         'username': result[1],
                        #         'profile_img': result[3],
                        #         'email': result[4],
                        #         'phone': result[5],
                        #         'userType': result[6],
                        #         'first_name': result[7],
                        #         'last_name': result[8],
                        #         'city': result[9],
                        #         # 'state': myresult[10],
                        #         'address': result[11],
                        #         'zipcode': result[12],
                        #         'gender': result[13],
                        #     }
                        # recognized_faces.append(person)
                    
                    # for (top, right, bottom, left), person in zip(face_locations, recognized_faces):
                    #     cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
                    #     cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
                    #     font = cv2.FONT_HERSHEY_DUPLEX
                    #     cv2.putText(frame, person['username'], (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)
                    for (top, right, bottom, left) in face_locations:
                        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
                        cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
                        font = cv2.FONT_HERSHEY_DUPLEX
                        cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)
                    
                    _, im_arr = cv2.imencode('.jpg', frame)  # im_arr: image in Numpy one-dim array format.
                    im_bytes = im_arr.tobytes()
                    im_b64 = base64.b64encode(im_bytes)
                    status = {"status":200, "base64_image":im_b64, "recognized_faces":recognized_faces}
                except Exception  as e:
                    print(str(e))
                    status = {"status":400, "result":str(e)}
                return jsonify(status)
            else:
                pass


@app.route('/video_feed/<cameraID>', methods=['get'])
def viewLiveVideo(cameraID):
    # liveObj['id'] = cameraID
    # liveObj['flag'] = 0
    return Response(gen(cameraID), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/facial_recognition/<cameraID>', methods = ['get'])
def get_facial_recognition(cameraID):

    return facial_recognition(cameraID)

@app.route('/temperature/<cameraID>', methods = ['get'])
def temperature(cameraID):
    image_path = "/home/r_praneeth/Desktop/flir-one-pro-thermal-image-1.jpg"
    while True:
        with open(image_path, 'rb') as f:
            check_chars = f.read()[-2:]
        if check_chars != b'\xff\xd9':
            pass
        else:
            if flir.check_for_thermal_image(image_path):
                frame = cv2.imread(image_path)
                if frame is not None:
                    flir.process_image(image_path)
                    temperatures_in_frame = flir.get_thermal_np()
                    face_locations = face_recognition.face_locations(frame)
                    try:   
                        temp_count = 0
                        for (top, right, bottom, left) in face_locations:
                            face_temp = temperatures_in_frame[top:bottom+1, left:right+1]
                            if np.amax(face_temp) > 37.22:
                                cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
                                cv2.putText(frame, "Temp:"+str(np.amax(face_temp))+"F", (left + 6, bottom - 6), cv2.FONT_HERSHEY_DUPLEX, 1.0, (255, 255, 255), 1)
                                count+=1
                        cv2.imshow("image",frame)
                        cv2.waitKey(0)
                        _, im_arr = cv2.imencode('.jpg', frame)  # im_arr: image in Numpy one-dim array format.
                        im_bytes = im_arr.tobytes()
                        im_b64 = base64.b64encode(im_bytes)
                        status = {"status":200, "base64_image":im_b64, "temperature_violations":temp_count}
                    except Exception  as e:
                        print(str(e))
                        status = {"status":400, "result":str(e)}
                    return jsonify(status)
                else:
                    pass
            else:
                print("No thermal Data")
                pass
    return jsonify({"status":400, "result":"Error"})

@app.route("/stoplive", methods=['post'])
@cross_origin()
def startStopFlag():
    try:
        capture.release()
        status = {"status": 200}
    except Exception as e:
        status = {"status": 500, "result": str(e)}
    return jsonify(status)

# API For Chart Data
@app.route('/violations', methods = ['get'])
@cross_origin()
def get_violations():
    try:
        sql_query = 'SELECT * FROM violation_records'
        cursor = con.cursor()
        cursor.execute(sql_query)
        results = cursor.fetchall()
        print(results)
        record = []
        for result in results:
            chart_data = {}
            chart_data['user_id'] = result[1]
            chart_data['date'] = result[2].strftime("%d-%b-%Y")
            chart_data['time'] = str(result[3])
            chart_data['violation'] = result[4]
            record.append(chart_data)
        status = {"status": 200, "result":record}
    except Exception as e:
        status = {"status": 400, "result":str(e)}
    return jsonify(status)

@app.route('/toggleCamera', methods = ['GET'])
@cross_origin()
def toggleCamera():
    try:
        global CAMERA
        toggle = request.args.get('toggle')
        if toggle == 'ON' and CAMERA == '':
            CAMERA = subprocess.Popen(['python3', 'record.py'])
            status = {"status":200, "data":"Process Created"}
        else:
            if CAMERA != '':
                CAMERA.kill()
                print(type(CAMERA))
                CAMERA = ''
            status = {"status":200, "data":"Process Deleted"}
    except Exception as e:
        status = {"status":400, "data":"Error : "+str(e)}
    return jsonify(status)



def updateSubPID(cameraID, processID):
    try:
        cur = con.cursor()
        sql = "UPDATE map_cameras SET sub_id = %s where id = %s"
        val = (processID, cameraID)
        cur.execute(sql, val)
        con.commit()
    except:
        print("Update log error")



def responseHandler(code, result):
    return jsonify({"status": code, "result": result})

if __name__ == '__main__':
    # threading.Thread(target=recordVideos, args=()).start()
    loadFaces()
    app.run(debug=True, host='0.0.0.0', port=3001)