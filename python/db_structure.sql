CREATE database `wellness`;

USE `wellness`;

CREATE TABLE `map_cameras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `sub_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `map_camera_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `camera_id` int(11) NOT NULL,
  `video_path` varchar(100) NOT NULL,
  `thumbail_path` varchar(100) NOT NULL,
  `video_duration` varchar(50) NOT NULL,
  `video_name` varchar(50) NOT NULL,
  `video_date` date NOT NULL,
  `start_time` varchar(50) NOT NULL,
  `end_time` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `profile_img` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT 0,
  `password` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `userType` int(11) DEFAULT 2,
  `gender` varchar(50) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zipcode` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `violation_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT 'UNKNOWN',
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `violation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `violation_input` CHECK (`violation` in ('Social Distance','Not Wearing Mask','Higher Temperature'))
);





