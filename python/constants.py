import logging
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import json 
import mysql.connector as MySQLdb

f = open('app.config.json')
config = json.load(f) 
f.close() 

logging.basicConfig(level=logging.DEBUG, filename='app.log',filemode='w', format='%(name)s - %(levelname)s - %(message)s')

class Constants:

    def send_email(self, arguments):
        logging.debug("send_email() Called")
        try:
            s = smtplib.SMTP(host=config['constants']["email_config"]["host"], port=config['constants']["email_config"]["port"])
            # s = smtplib.SMTP(host='smtp.gmail.com', port=587)
            # s = smtplib.SMTP(host='smtp.office365.com', port=587)

            s.starttls()
            s.login(config['constants']["email_config"]["email_id"], config['constants']["email_config"]["password"])

            for recipient in arguments['To List'].split(','):
                print(recipient)
                msg = MIMEMultipart()
                msg['From'] = config['constants']["email_config"]["email_id"]
                msg['To'] = recipient
                msg['Subject'] = arguments['Subject']
                msg.attach(MIMEText(arguments['Body'],'plain'))
                s.send_message(msg)
            s.quit()
            logging.debug("E-Mail Sent Successfully")
        except Exception as e:
            logging.error("Email Failed to Send. ERROR:", e)

    def write_to_file(self, arguments):
        logging.debug("write_to_file() Called")
        f = open(arguments['Path'],'w')
        f.write(arguments['Value'])
        f.close()
        
    def database_query(self, arguments):
        logging.debug("Database_Query() Called")
        if arguments['Database Type'] == 'MySQL':
            host = arguments['Server']
            user = arguments['Username']
            password = arguments['Password']
            db = arguments['Database Instance Name']
            sql = MySQLdb.connect(host=host, user=user, password=password,
                   db=db, use_unicode=True, charset='utf8')
            cursor = sql.cursor()
            try:
                cursor.execute(arguments['SQL Query'])
                sql.commit()
                logging.info("Inserted Successfully")
            except:
                logging.error("Insertion Failed, Rolling Back")
    
    def database_insert(self, arguments):
        logging.debug("Database_Insert() Called")
        if arguments['Database Type'] == 'MySQL':
            host = arguments['Server']
            user = arguments['Username']
            password = arguments['Password']
            db = arguments['Database Instance Name']
            sql = MySQLdb.connect(host=host, user=user, password=password,
                   db=db, use_unicode=True, charset='utf8')
            cursor = sql.cursor()
            try:
                cursor.execute(arguments['SQL Query'])
                sql.commit()
                logging.info("Inserted Successfully")
            except:
                logging.error("Insertion Failed, Rolling Back")
    
    def execute_script(self, arguments):
        logging.debug("execute_script() Called")
        try:
            subprocess.call(["python3", arguments['Path to Script']])
            logging.debug("Script executed successfully")
        except Exception as e:
            logging.error("Error:"+str(e))

    def send_text(self, arguments):
        logging.debug("send_text() Called")

    def trigger_LED(self, arguments):
        logging.debug("trigger_LED() Called")

    def web_service_call(self, arguments):
        logging.debug("web_service_call() Called")

