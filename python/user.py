
import flask
from flask import Blueprint, Flask, render_template, request, send_from_directory, send_file
from flask import jsonify
from flask_cors import CORS
from flask_cors import cross_origin
from datetime import datetime
import mysql.connector as sql
# import config as cfg
from passlib.hash import pbkdf2_sha256
import json
from flask_paginate import Pagination, get_page_args
import re
import base64
import numpy as np
import cv2
import os
import face_recognition

host = 'localhost'
user = 'root'
password = 'Praneeth123'
db = 'wellness'
mydb = sql.connect(host=host, user=user, password=password,
                   db=db, use_unicode=True, charset='utf8')
cursor = mydb.cursor(buffered=True)


app = Blueprint('app', __name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})


def validate_username(name):
    sql_query = "SELECT username FROM users"
    cursor.execute(sql_query)
    usernames = cursor.fetchall()
    for username in usernames:
        if username[0] == name:
            return True
    return False

def validate_email(email):
    match = re.search(r'\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b', str(email), re.I)
    if match is not None:
        return False
    else:
        return True

@app.route('/create', methods=['POST'])
@cross_origin()
def create():
    first_name = request.form.get("first_name")
    last_name = request.form.get("last_name")
    phone = request.form.get("phone")
    password = request.form.get("password")
    email = request.form.get("email")
    address = request.form.get("address")
    city = request.form.get("city")
    zipcode = request.form.get("zipcode")
    state = request.form.get("state")
    dob = request.form.get("dob")
    gender = request.form.get("gender")
    username = first_name+" "+last_name
    if validate_username(username):
        return jsonify({"status": 400, "message": "Username Already Exists."})
    
    if validate_email(email):
        return jsonify({"status": 400, "message": "Enter a Valid email"})
    hashPassword = pbkdf2_sha256.hash(password)
    sql_query = "INSERT into users (username,password,phone,email,first_name,last_name,city,state,address,zipcode,gender,dob ) values ('%s', '%s', '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (
            username, hashPassword, phone, email,first_name,last_name,city,state,address,zipcode,gender,dob)
    print(sql_query)
    try:
        cursor.execute(sql_query)
        mydb.commit()
        return jsonify({"status": 200, "message": "User created successfully"})
    except Exception as e:
        return jsonify({"status": 400, "message": "Something went wrong"+str(e)})

@app.route('/editProfile', methods=['POST'])
@cross_origin()
def editProfile():
    address=request.form.get("address")
    user_id=request.form.get("user_id")
    username = request.form.get("username")
    phone = request.form.get("phone")
    zipcode = request.form.get("zipcode")
    city = request.form.get("city")
    state = request.form.get("state")
    gender = request.form.get("gender")
    # profile_img = request.form.get("profile_img")
    print(address,user_id)
    sql_query = "UPDATE users SET phone=%s,address='%s',zipcode='%s',city='%s',state='%s',gender ='%s' WHERE id = %d" % (phone,address,zipcode,city,state,gender,int(user_id))
    print(sql_query)

    try:
        cursor.execute(sql_query)
        mydb.commit()
        return jsonify({"status": 200, "message": "User Updated successfully"})
    # except:
    #     return jsonify({"status": 400, "message": "Something went wrong"})
    except Exception as e:
        status = {"status": 202, "result": str(e)}
        return responseHandler(202,status)

@app.route('/user_profile', methods=['get'])
@cross_origin()
def user_profile():
    user_id = request.args.get("user_id")
    print(user_id)
    if user_id:
        sql_query = "SELECT id,username,password,profile_img,email,phone,userType,first_name,last_name,city,state,address,zipcode,gender,dob FROM users where id = %d " % (
            int(user_id))
        print(sql_query)
        cursor.execute(sql_query)
        myresult = cursor.fetchone()

        if myresult:
            resultObj = {
                'id': myresult[0],
                'username': myresult[1],
                'profile_img': myresult[3],
                'email': myresult[4],
                'phone': myresult[5],
                'userType': myresult[6],
                'first_name': myresult[7],
                'last_name': myresult[8],
                'city': myresult[9],
                'state': myresult[10],
                'address': myresult[11],
                'zipcode': myresult[12],
                'gender': myresult[13],
                'dob': myresult[14],
                'token':myresult[0],
            }
            return responseHandler(200, resultObj)
        else:
            return responseHandler(202, "User Not Found!")
    else:
        return responseHandler(202, "UserId is mandatory")



@app.route('/', methods=['post','OPTIONS'])
@cross_origin()
def test():
    return responseHandler(202,"success")

@app.route('/login', methods=['post'])
@cross_origin()
def login():
    
    password = request.form.get('password')
    email = request.form.get("email")
    try:
        sql_query = "SELECT id,username,password,profile_img,email,phone,userType,first_name,last_name FROM users where email = '%s'" % (str(email))
        cursor.execute(sql_query)
        myresult = cursor.fetchone()
        if myresult and len(myresult)>0:
            resultObj = {
                'id': myresult[0],
                'username': myresult[1],
                'profile_img': myresult[3],
                'email': myresult[4],
                'phone': myresult[5],
                'userType': myresult[6],
                'first_name': myresult[7],
                'last_name': myresult[8],
                'token':myresult[0],
                 
            }
            userPassword = myresult[2]
            if password and resultObj and userPassword:
                if pbkdf2_sha256.verify(password, userPassword):
                    return responseHandler(200, resultObj)
                else:
                    return responseHandler(202, "Wrong Password!!!")
            else:
                return responseHandler(202, "Something Went Wrong")
        else:
            return responseHandler(202, "User Not Found!")
    except Exception as e:
        return responseHandler(400,str(e))

@app.route('/changePassword', methods=['put'])
@cross_origin()
def changePassword():
    userId = request.form.get('user_id')
    current_password = request.form.get('current_password')
    new_password = request.form.get('new_password')
    confirm_new_password = request.form.get('confirm_new_password')
    hashPassword = pbkdf2_sha256.hash(new_password)
    if confirm_new_password == new_password:
        sql_query = "SELECT username,password, email FROM users where id = '%d' " % (
            int(userId))

        cursor.execute(sql_query)
        myresult = cursor.fetchone()
        if myresult and pbkdf2_sha256.verify(current_password,  myresult[1]):
            sql_query = "UPDATE users SET password  = '%s' where id =  '%d' " % (
                hashPassword, int(userId))
            cursor.execute(sql_query)
            mydb.commit()
            return jsonify({"status": 200, "message": "Password Changed successfully"})
        else:
            return jsonify({"status": 400, "message": "Password change Unsuccessful, verify credentials"})
    else:
        return jsonify({"status": 202, "message": "new password and confirm password should match"})


@app.route('/delete', methods=['delete'])
@cross_origin()
def delete_User():
    userId = request.args.get("user_id")
    try:
        sql_query = "DELETE FROM users where id = '%d' " % (int(userId))
        print(sql_query, "@@@@@@@@@@@@@@")

        cursor.execute(sql_query)
        mydb.commit()
        return jsonify({"status": 200, "message": "User deleted successfully"})
    except:
        return jsonify({"status": 400, "message": "User deletion failed"})

@app.route('/set_profile_pic', methods=['POST'])
@cross_origin()
def set_profile_pic():
    try:
        print("po")
        base64img = request.form.get('profile_picture')
        name = request.form.get('username')
        user_id = request.form.get('user_id')
        print(name,user_id,base64img)
        path = "./profile_pictures/"+str(name)+".jpg"
        # pic = pic.read()
        # numpyimg = np.frombuffer(pic, np.uint8)
        # orgImg = cv2.imdecode(numpyimg, cv2.IMREAD_COLOR)
        data = re.sub('^data:image/.+;base64,', '', base64img)
        data = base64.b64decode(data)
        frame = np.fromstring(data, np.uint8)
        orgImg = cv2.imdecode(frame, cv2.IMREAD_COLOR)
        print(name,user_id,base64img)
        sql_query = f"UPDATE users SET profile_img ='{path}' WHERE id = {int(user_id)}"
        cursor.execute(sql_query)
        mydb.commit()
        if not os.path.exists(path):
            cv2.imwrite(path,orgImg)
            return jsonify({"status": 200, "message": "Profile Picture Added"})
        else:
            os.remove(path)
            cv2.imwrite(path,orgImg)
            return jsonify({"status": 200, "message": "Profile Picture Updated"})
    except Exception as e:
        return jsonify({"status": 400, "message": "Error : "+str(e)})

@app.route('/get_profile_pic', methods=['GET'])
def mask_image():
    # folder_dir = '../.'
    path = request.args.get('image', default="")
    image = os.path.abspath(path)
    return send_file(image, mimetype='image/jpg')

@app.route('/getUsers', methods=['get'])
@cross_origin()
def userList():
    dict_list = {}
    pagination = {}
    user_list = []
    limit = int(request.args.get("limit"))
    page = int(request.args.get("pagenumber"))
    offset = (page-1) * limit + 1 if page > 1 else 1
    offset = offset-1
    # sql_query = "SELECT id,username,profile_img,email,phone FROM users offset 1 "
    # sql_query = "SELECT id,username,profile_img,email,phone FROM users"
    #  ORDER BY id LIMIT  '%d'  OFFSET  '%d' " %(int(limit),int(pagenumber))
    sql_query = "SELECT id,username,profile_img,email,phone FROM map.users LIMIT {} OFFSET {}".format( limit, offset)
    print(sql_query)
    cursor.execute(sql_query)
    myresult = cursor.fetchall()
    for x in myresult:
        user_list.append({
            'id': x[0],
            'username': x[1],
            'profile_img': x[2],
            'email': x[3],
            'phone': x[4]
        })

    dict_list["users"] = user_list
    # dict_list["pagination"] = pagination

    return responseHandler(200, dict_list)


def responseHandler(code, result):
    return jsonify({"status": code, "result": result})