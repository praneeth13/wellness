-- MariaDB dump 10.17  Distrib 10.5.3-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: map
-- ------------------------------------------------------
-- Server version	10.5.3-MariaDB-1:10.5.3+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `map_camera_videos`
--

DROP TABLE IF EXISTS `map_camera_videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `map_camera_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `camera_id` int(11) NOT NULL,
  `video_path` varchar(100) NOT NULL,
  `thumbail_path` varchar(100) NOT NULL,
  `video_duration` varchar(50) NOT NULL,
  `video_name` varchar(50) NOT NULL,
  `video_date` date NOT NULL,
  `start_time` varchar(50) NOT NULL,
  `end_time` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `map_camera_videos`
--

LOCK TABLES `map_camera_videos` WRITE;
/*!40000 ALTER TABLE `map_camera_videos` DISABLE KEYS */;
INSERT INTO `map_camera_videos` VALUES (1,1,'videos/1/2020-06-18 20:57:54.408371.webm','thumbail','2','2020-06-18 20:57:54.408371','2020-06-18','08:57 PM','09:02 PM'),(2,1,'videos/1/2020-06-18 21:21:24.201007.webm','thumbail','2','2020-06-18 21:21:24.201007','2020-06-18','09:21 PM','09:26 PM'),(3,1,'videos/1/2020-06-18 21:26:29.337643.webm','thumbail','2','2020-06-18 21:26:29.337643','2020-06-18','09:26 PM','09:31 PM'),(4,1,'videos/1/2020-06-18 21:31:56.867145.webm','thumbail','2','2020-06-18 21:31:56.867145','2020-06-18','09:31 PM','09:36 PM'),(5,1,'videos/1/2020-06-18 21:37:17.225238.webm','thumbail','2','2020-06-18 21:37:17.225238','2020-06-18','09:37 PM','09:42 PM'),(6,1,'videos/1/2020-06-18 21:42:17.661674.webm','thumbail','2','2020-06-18 21:42:17.661674','2020-06-18','09:42 PM','09:47 PM');
/*!40000 ALTER TABLE `map_camera_videos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `map_cameras`
--

DROP TABLE IF EXISTS `map_cameras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `map_cameras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `sub_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `map_cameras`
--

LOCK TABLES `map_cameras` WRITE;
/*!40000 ALTER TABLE `map_cameras` DISABLE KEYS */;
INSERT INTO `map_cameras` VALUES (1,'Vimal Camera',1,'http://192.168.1.177:5002/video_feed','39.7409863558836','-104.9908447265625',1,'Active',NULL,'6505'),(2,'John',1,'http://192.168.1.180:5002/video_feed','39.5165250801941','-106.4024400875667',1,'Active John',NULL,'80693');
/*!40000 ALTER TABLE `map_cameras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `profile_img` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT 0,
  `password` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `userType` int(11) DEFAULT 2,
  `gender` varchar(50) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varbinary(100) DEFAULT NULL,
  `zipcode` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'praneeth r',NULL,NULL,NULL,0,'$pbkdf2-sha256$29000$sdb6n9P6P8f4v1dq7R2DkA$ZbKLjq8GFy4DmLswfz9h9p57tvKJmVdFFr.wzd5D.G0','praneeth','r',822051704,'praneethcruzon@gmail.com',2,NULL,NULL,NULL,NULL),(4,'srinithi s',NULL,NULL,NULL,0,'$pbkdf2-sha256$29000$FOI8J8T4H8M4R4hRaq117g$loNqaDUtft1N0Gv0gmqJQP1RgJlSitVXHDUqfjKw2fc','srinithi','s',822051704,'praneethcruzon@gmail.com',2,NULL,NULL,NULL,NULL),(6,'john s',NULL,NULL,NULL,0,'$pbkdf2-sha256$29000$aO299x4D4Pwf41yrNUboHQ$s.bKQxM5waby.g5Vuj3.4TRCYeoRKZ5nQXVAMDfmwG8','john','s',822051704,'john.marinton@optisolbusiness.com',2,NULL,NULL,NULL,NULL),(10,'Durai Vinod',NULL,NULL,NULL,0,'$pbkdf2-sha256$29000$0RqDUMoZQ2jtPadUqpVSSg$SSjGhsLaztneHyyI2JOrvqoP40Ypy/tr6tU8GKL6lHk','Durai','Vinod',999999999,'duraivinoth001@gmail.com',2,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `violation_records`
--

DROP TABLE IF EXISTS `violation_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `violation_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT 'UNKNOWN',
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `violation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `violation_input` CHECK (`violation` in ('Social Distance','Not Wearing Mask'))
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `violation_records`
--

LOCK TABLES `violation_records` WRITE;
/*!40000 ALTER TABLE `violation_records` DISABLE KEYS */;
INSERT INTO `violation_records` VALUES (1,'4','2020-06-17','06:40:00','Social Distance'),(2,'6','2020-06-17','06:42:00','Not Wearing Mask');
/*!40000 ALTER TABLE `violation_records` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-19 19:24:24
