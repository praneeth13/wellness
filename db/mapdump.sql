/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 8.0.20-0ubuntu0.20.04.1 : Database - map
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`map` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `map`;

/*Table structure for table `map_camera_videos` */

DROP TABLE IF EXISTS `map_camera_videos`;

CREATE TABLE `map_camera_videos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `camera_id` int NOT NULL,
  `video_path` varchar(100) NOT NULL,
  `thumbail_path` varchar(100) NOT NULL,
  `video_duration` varchar(50) NOT NULL,
  `video_name` varchar(50) NOT NULL,
  `video_date` date NOT NULL,
  `start_time` varchar(50) NOT NULL,
  `end_time` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `map_camera_videos` */

/*Table structure for table `map_cameras` */

DROP TABLE IF EXISTS `map_cameras`;

CREATE TABLE `map_cameras` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `type` tinyint DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `is_active` tinyint DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `sub_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `map_cameras` */

insert  into `map_cameras`(`id`,`name`,`type`,`address`,`latitude`,`longitude`,`is_active`,`notes`,`state`,`sub_id`) values 
(1,'Vimal Camera',1,'http://192.168.1.177:5002/video_feed','39.7409863558836','-104.9908447265625',1,'Active',NULL,'14804'),
(2,'John',1,'http://192.168.1.180:5002/video_feed','39.5165250801941','-106.4024400875667',1,'Active John',NULL,'80693');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `profile_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `is_delete` tinyint DEFAULT '0',
  `password` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone` int DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `userType` int DEFAULT '2',
  `gender` varchar(50) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varbinary(100) DEFAULT NULL,
  `zipcode` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`profile_img`,`address`,`dob`,`is_delete`,`password`,`first_name`,`last_name`,`phone`,`email`,`userType`,`gender`,`city`,`state`,`zipcode`) values 
(1,'DuraiVinoth','/static/DuraiVinoth.jpg','some address',NULL,0,'$pbkdf2-sha256$29000$XIvx/h.DMMbYm9NaC4HwPg$GOgHOSSmU9pLM/FqyWzO3MLeV7eozRoWCjF1iE.0dmc','Durai','Vinoth',867587229,'duraivinoth001@gmail.com',2,'Male','Chennai','tamilnadu',600132);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;